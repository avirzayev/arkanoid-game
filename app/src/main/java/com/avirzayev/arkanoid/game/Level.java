package com.avirzayev.arkanoid.game;

import java.io.Serializable;

public class Level implements Serializable {

    private String name;
    private int id;
    private boolean userLike;
    private String creator;
    private int likes;
    private boolean editable;
    private int[][] level;

    public Level(int id, String name, String creator, int likes, String level) {
        this.id = id;
        this.name = name;
        this.creator = creator;
        this.likes = likes;
        this.level = convertStringToLevel(level);

    }

    public Level(int id, String name, String creator, int likes, int[][] level) {
        this.id = id;
        this.name = name;
        this.creator = creator;
        this.likes = likes;

        this.level = level;

    }

    public Level(int id, String name, String creator, int likes) {
        this.id = id;
        this.name = name;
        this.creator = creator;
        this.likes = likes;


    }

    public int[][]  convertStringToLevel(String level){

        String arr[] = level.split("b");

        int[][] levelArr = new int[arr.length][6];

        for (int j = 0; j < arr.length; j++) {
            String str = arr[j];
            for(int i = 0; i < str.length(); i++){
                levelArr[j][i] = Integer.parseInt(str.charAt(i)+"");
            }
        }

        return levelArr;

    }

    public static String convertGridToString(int[][] level){
        String result = "";

        if (level == null) return "";

        for(int r = 0; r < level.length; r++){
            for(int c = 0; c < level[r].length; c++){
                result += level[r][c];
            }
            result += "b";
        }

        result = result.substring(0, result.length() - 1);

        return result;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCreator() {
        return creator;
    }

    public int getLikes() {
        return likes;
    }

    public int[][] getLevel() {
        return level;
    }

    public void addLike(int like){
        likes += like;
    }

    public boolean isLiked(){
        return userLike;
    }

    public void setUserLike(boolean userLike) {
        this.userLike = userLike;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLevel(int[][] level) {
        this.level = level;
    }
}
