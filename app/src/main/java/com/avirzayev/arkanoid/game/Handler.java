package com.avirzayev.arkanoid.game;

import android.graphics.Canvas;

import com.avirzayev.arkanoid.gameobjects.GameObject;
import com.avirzayev.arkanoid.gameobjects.Renderable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;


/** Handles all the rendeable objects in the game **/
public class Handler {

    private static ArrayList<Renderable> renderables = new ArrayList<>(); // list of all the objects
    private static ArrayList<Renderable> deleteArrays = new ArrayList<>(); // queue for deleting the unnecessary objects


    /** iterates through all the objects and updates them*/
    public static void update(float delta){
        for(Renderable renderable: renderables){
            renderable.update(delta);
        }

        fetch();

    }

    /** iterates through all the objects and draws them*/
    public static void draw(Canvas canvas){
        for(Renderable renderable: renderables){
            renderable.draw(canvas);
        }
    }

    /** remove a certain object **/
    public static void remove(Renderable renderable){
        deleteArrays.add(renderable);
    }

    /** removes the objects in queue **/
    private static void fetch(){
        renderables.removeAll(deleteArrays);

        deleteArrays.clear();
    }

    /** adds objects to the list **/
    public static void addObject(Renderable renderable){
        renderables.add(renderable);
    }

    public static void addObject(Renderable... arr){
        Collections.addAll(renderables, arr);
    }


    public static void clear(){
        renderables.clear();
    }

    public static ArrayList<Renderable> getRenderables() {
        return renderables;
    }
}
