package com.avirzayev.arkanoid.game;

import android.app.Activity;

import com.avirzayev.arkanoid.dialogs.GameOverDialog;
import com.avirzayev.arkanoid.dialogs.LevelCompletedDialog;
import com.avirzayev.arkanoid.gameobjects.Ball;
import com.avirzayev.arkanoid.gameobjects.Panel;
import com.avirzayev.arkanoid.utilities.BrickCollection;
import com.avirzayev.arkanoid.utilities.DragEventHandler;

import java.util.Arrays;
import java.util.LinkedList;

public class LevelHandler {

    public static LinkedList<Level> levelsSet;

    private static int score; // score count
    private static int level = 0; // current level
    private static int ballsLeft = 3; // lives

    private static boolean levelStarted; // is the level started
    private static boolean isLevelGoing; // is the level runs

    // score constants
    public static final int SCORE_PER_PANEL_HIT = 1;
    public static final int SCORE_PER_BRICK_HIT = 5;


    // initialize the level
    public static void initializeLevel(){

        //put all the objects in place
        setup();

        level++; // increasing the level

        if(level > levelsSet.size()){
            ((Activity)GameView.gameView.getContext()).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    LevelCompletedDialog d = new LevelCompletedDialog(GameView.gameView.getContext());

                    d.show();

                }
            });
        }

        // generating new collection of bricks
        GameView.brickCollection = BrickCollection.generateBricks(levelsSet.get(level-1).getLevel());
    }

    // resetting the whole game stats
    public static void initializeGame(){
        ballsLeft = 3;
        score = 0;
        levelStarted = false;
        isLevelGoing = false;
        level =0;
        initializeLevel();
    }

    // putting all the objects in their place
    public static void setup(){
        Ball ball = GameView.ball;
        Panel panel = GameView.panel;

        // stopping the ball
        ball.getVector().resetMovement();

        // putting the panel in center
        panel.getVector().setPosX((GameView.WIDTH-panel.getWidth())/2);

        // putting the ball in center above the panel
        ball.getVector().setPosX(GameView.WIDTH/2);
        ball.getVector().setPosY(panel.getVector().getPosY() - Ball.RADIUS*1.5f);

        // clear the drag event from object
        DragEventHandler.clearEvent();
    }

    // runs periodically, handles the level in loop
    public static void handleLevel(){

        // do nothing if the game ended or not started
        if (!levelStarted || ballsLeft == 0) return;

        // if there is no bricks left, move to the next level
        if(GameView.brickCollection.getBreakableSize() <= 0){
            isLevelGoing = false;
            levelStarted = false;
            initializeLevel();
        }
    }

    // increasing the scores
    public static void increasePanelHitScore(){
        score += SCORE_PER_PANEL_HIT;
    }

    public static void increaseBrickHitScore(){
        score += SCORE_PER_BRICK_HIT;
    }

    // getters
    public static int getScore() {
        return score;
    }

    public static int getLevel() {
        return level;
    }

    // ball loss handling
    public static void ballLoss(){

        ballsLeft -= 1;

        // if the game ended enable a dialog
        if(isGameOver()){
            ((Activity)GameView.gameView.getContext()).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    GameOverDialog d = new GameOverDialog(GameView.gameView.getContext());

                    d.show();

                }
            });
        }

        // level needs to start again
        isLevelGoing = false;
        levelStarted = false;

        // reset the position
        setup();
    }

    public static void setLevelStarted(boolean levelStarted) {

        // if level already started don't do anything
        if(isLevelGoing) return;

        isLevelGoing = true;
        LevelHandler.levelStarted = levelStarted;

        Ball ball = GameView.ball;
        ball.randomizeVelocityDirection();

        Panel panel = GameView.panel;

        // requesting drag event
        DragEventHandler.requestEventFor(panel, DragEventHandler.DragAxis.X_AXIS);
    }


    public static void setLevelsSet(Level level){
        LinkedList<Level> list = new LinkedList<>();
        list.add(level);
        levelsSet = list;
    }

    public static void setLevelsSet(Level... level){
        levelsSet = new LinkedList<>(Arrays.asList(level));
    }

    public static void setLevelsSet( LinkedList<Level> list ){
        levelsSet = list;
    }



    public static int getBallsLeft() {
        return ballsLeft;
    }

    // game over when no balls left
    public static boolean isGameOver(){
        return ballsLeft <= 0;
    }
}
