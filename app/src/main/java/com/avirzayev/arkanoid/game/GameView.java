package com.avirzayev.arkanoid.game;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;

import com.avirzayev.arkanoid.dialogs.PauseDialog;
import com.avirzayev.arkanoid.gameobjects.Ball;
import com.avirzayev.arkanoid.gameobjects.Brick;
import com.avirzayev.arkanoid.gameobjects.Panel;
import com.avirzayev.arkanoid.utilities.BrickCollection;
import com.avirzayev.arkanoid.utilities.DragEventHandler;
import com.avirzayev.arkanoid.utilities.DrawableButton;
import com.avirzayev.arkanoid.utilities.HeadUpDisplay;
import com.avirzayev.arkanoid.utilities.ImageHandler;
import com.avirzayev.arkanoid.utilities.Vector;

public class GameView extends CustomView{


    public static GameView gameView; // instance

    private static Bitmap background; // game backgrounds
    private boolean pause; // if the game is paused

    public static Ball ball; // the ball object
    public static Panel panel; // panel object

    public static BrickCollection brickCollection; // collection of bricks

    private DrawableButton pauseBTN; // pause button

    public GameView(Context context) {
        super(context);


        setFocusable(true);

        gameView = this;

    }

    // creating all the objects for the game
    public void initializeGame(){

        // clearing the handler from objects
        Handler.clear();

        // panel
        panel = new Panel(new Vector( (WIDTH - Panel.PANEL_WIDTH)/2,HeadUpDisplay.LOWER_DIMENSIONS_Y  - Panel.PANEL_HEIGHT));

        // ball
        ball = new Ball((WIDTH - Ball.RADIUS)/2,(HEIGHT)/2 + Ball.RADIUS*4, panel);

        // brick collection
        brickCollection = BrickCollection.generateBricks(Levels.LEVEL_1);

        // pause button
        pauseBTN = new DrawableButton(new Vector((HeadUpDisplay.UPPER_DIMENSIONS_WIDTH - WIDTH/10)/2,(HeadUpDisplay.UPPER_DIMENSIONS_HEIGHT - WIDTH/10)/2 ), WIDTH/10, WIDTH/10, null);

        //setting listener
        pauseBTN.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                pauseGame();
            }
        });

        // setting image to button
        pauseBTN.setBitmap(Bitmap.createScaledBitmap(ImageHandler.getImage("pause"), (int)pauseBTN.getWidth(), (int)pauseBTN.getHeight(), true));

        // adding all the object to the handler
        Handler.addObject(panel, ball, new HeadUpDisplay(0,0,0,0), pauseBTN);

        // initialize the level
        LevelHandler.initializeGame();

    }



    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        if(gameThread.isAlive()) return;

        super.surfaceCreated(surfaceHolder);


        // loading images
        ImageHandler.loadImages(getContext());

        // initializing sizes
        initGameSizes();

        // game background
        background = Bitmap.createScaledBitmap(ImageHandler.getImage("wall"), (int)WIDTH+ 10, (int)HEIGHT + 10, true);

        initializeGame();
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {


    }


    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);

        if (canvas == null) return;

        // rendering the bitmap and the objects

        canvas.drawBitmap(background,0,0, new Paint());
        Handler.draw(canvas);
        brickCollection.draw(canvas);

    }

    public void update(float delta) {

        // don't update if the game is paused
        if (gameView.isPause()) return;

        Handler.update(delta);

        brickCollection.update(delta, ball);

        LevelHandler.handleLevel();


    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        // if the game is over or paused don't enable touching
        if (LevelHandler.isGameOver() || isPause()) return false;

        float ex = event.getX();

        float ey = event.getY();

        switch(event.getAction())
        {
            case MotionEvent.ACTION_DOWN:

                // if the player clicks on the screen start the game -> works once
                LevelHandler.setLevelStarted(true);

                // enable the drag that assigned to the object
                DragEventHandler.enableDrag(ex,ey);

                // check if the button was clicked
                pauseBTN.updateClick(ex,ey);


                break;
            case MotionEvent.ACTION_MOVE:

                // update the drag
                DragEventHandler.updateDragCoords(ex, ey);

                break;
            case MotionEvent.ACTION_UP:
                // disable the drag
                DragEventHandler.disableDrag();
                break;
            default:
                break;
        }
        return true;
    }

    // end the thread
    public void endGame(){
        gameThread.setRunning(false);
        gameThread.interrupt();

    }

    public boolean isPause() {
        return pause;
    }

    public void setPause(boolean pause) {
        this.pause = pause;
    }

    public void pauseGame(){
        setPause(true);
        new PauseDialog(getContext(),true).show();

    }
}
