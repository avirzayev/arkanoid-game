package com.avirzayev.arkanoid.game;

import android.app.Dialog;
import android.content.Context;
import android.graphics.PixelFormat;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.avirzayev.arkanoid.gameobjects.Ball;
import com.avirzayev.arkanoid.gameobjects.Brick;
import com.avirzayev.arkanoid.gameobjects.Panel;
import com.avirzayev.arkanoid.utilities.HeadUpDisplay;

public abstract class CustomView extends SurfaceView implements SurfaceHolder.Callback {

    protected GameThread gameThread;

    public static float WIDTH, HEIGHT; // game width and height


    public CustomView(Context context) {
        super(context);


        gameThread = new GameThread(getHolder(), this);

        getHolder().addCallback(this);




    }

    public abstract void update(float delta);

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

        // if the thread isn't on -> turn it
        if(!gameThread.isAlive()){

            // fixing color rendering
            holder.setFormat(PixelFormat.TRANSLUCENT);

            gameThread.setRunning(true);
            gameThread.start();



            // getting the screen dimensions
            WIDTH = holder.getSurfaceFrame().right;
            HEIGHT = holder.getSurfaceFrame().bottom;

        }

    }

    // initialize game sizes according to screen dimension
    public static void initGameSizes(){
        Panel.PANEL_WIDTH = WIDTH/3f;
        Panel.PANEL_HEIGHT = Panel.PANEL_WIDTH/4;

        Ball.RADIUS = WIDTH/40;

        Brick.BRICK_WIDTH = WIDTH/(Levels.BRICKS_PER_ROW + 0.5f);
        Brick.BRICK_HEIGHT = Brick.BRICK_WIDTH/3;
        Brick.PADDING = Brick.BRICK_WIDTH/12;

        HeadUpDisplay.UPPER_DIMENSIONS_WIDTH = WIDTH;
        HeadUpDisplay.UPPER_DIMENSIONS_HEIGHT = HEIGHT/10;

        HeadUpDisplay.UPPER_DIMENSIONS_Y = 0;


        HeadUpDisplay.LOWER_DIMENSIONS_WIDTH = WIDTH;
        HeadUpDisplay.LOWER_DIMENSIONS_HEIGHT = HEIGHT/10;

        HeadUpDisplay.LOWER_DIMENSIONS_Y = HEIGHT*(9/10f);
    }
}
