package com.avirzayev.arkanoid.game;

import java.util.LinkedList;

// holds levels grid
public class Levels {

    public static final int BRICKS_PER_ROW = 6;

    public static final int[][] LEVEL_1 = {{1,1,1,1,1,1},
                                           {1,1,1,1,1,1},
                                           {1,1,1,1,1,1}};

    public static final int[][] LEVEL_2 = {{1,1,1,1,1,1},
                                           {1,1,1,1,1,1},
                                           {1,1,1,1,1,1},
                                           {3,3,1,1,3,3}};

    public static final int[][] LEVEL_3 = {{1,1,1,1,1,1},
                                           {1,3,3,3,3,1},
                                           {1,0,1,1,0,1},
                                           {1,0,1,1,0,1},
                                           {1,1,2,2,1,1}};

    public static final int[][] LEVEL_4 = {{1,1,1,1,1,1},
                                           {1,3,1,1,3,1},
                                           {1,3,3,3,3,1},
                                           {1,2,3,3,2,1},
                                           {1,2,2,2,2,1},
                                           {1,1,1,1,1,1}};

    public static final int[][] LEVEL_5 = {{1,3,3,3,3,1},
                                           {1,2,3,3,2,1},
                                           {1,2,3,3,2,1},
                                           {1,2,3,3,2,1},
                                           {1,3,3,3,3,1},
                                           {1,2,2,2,2,1}};
    public static final int[][][] LEVEL_LIST = {LEVEL_1,LEVEL_2,LEVEL_3, LEVEL_4, LEVEL_5};


    public static LinkedList<Level> getLevels(){
        LinkedList<Level> levels = new LinkedList<>();

        for (int[][] l : LEVEL_LIST){
            Level level = new Level(0, "", "", 0, l);
            levels.add(level);
        }

        return levels;
    }


}
