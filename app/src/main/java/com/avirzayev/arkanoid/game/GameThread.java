package com.avirzayev.arkanoid.game;

import android.graphics.Canvas;
import android.os.SystemClock;
import android.view.SurfaceHolder;

// Thread that runs the game
public class GameThread extends Thread {

    private SurfaceHolder surfaceHolder;
    private CustomView gameView;

    private boolean running; // indicates if the thread is running
    public static Canvas canvas; // game canvas

    private double lastUpdateTime; // last time before the loop updated

    public static final float TARGET_FPS = 60; // target fps for the game to run

    public static final float TARGET_DELTA_TIME = 1/TARGET_FPS; // update frequency

    private float accumulator = 0; // counts how much time is leaked from the update

    public GameThread(SurfaceHolder surfaceHolder, CustomView gameView) {

        super();
        this.surfaceHolder = surfaceHolder;
        this.gameView = gameView;

    }

    @Override
    public void run() {
        while (running) {
            canvas = null;

            try {
                canvas = this.surfaceHolder.lockCanvas();

                synchronized(surfaceHolder) {

                    // getting how much time passed from the last update
                    float delta = getDeltaTime(SystemClock.elapsedRealtime());

                    // adding the time to the accumulator
                    accumulator += delta;

                    // update the game until the target frequency is achieved
                    while(accumulator >= TARGET_DELTA_TIME){
                        this.gameView.update(TARGET_DELTA_TIME);
                        accumulator -= TARGET_DELTA_TIME;
                    }

                    // draw the game
                    this.gameView.draw(canvas);
                }
            } catch (Exception e) {} finally {
                if (canvas != null) {
                    try {
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    // set running
    public void setRunning(boolean isRunning) {
        running = isRunning;

        if(running){
            updateLastUpdateTime();
        }
    }

    // updating last time
    private void updateLastUpdateTime(){
        lastUpdateTime = SystemClock.elapsedRealtime();
    }

    // returns how much time passed since last update
    private float getDeltaTime(double currentMills){

        float delta = (float) (currentMills - lastUpdateTime)/1000;

        lastUpdateTime = currentMills;

        return delta;
    }
}
