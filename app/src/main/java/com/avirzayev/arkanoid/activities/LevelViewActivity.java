package com.avirzayev.arkanoid.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.avirzayev.arkanoid.R;
import com.avirzayev.arkanoid.game.Level;
import com.avirzayev.arkanoid.game.LevelHandler;
import com.avirzayev.arkanoid.networking.NetworkRequests;
import com.avirzayev.arkanoid.utilities.SoundsHandler;

public class LevelViewActivity extends AppCompatActivity {

    private TextView levelNameTV, levelCreatorTV, levelLikesTV, likeBTNText;
    private ImageView likeBTNImage;
    private Button playBTN;
    private LinearLayout editButton, likeButton;
    private Level level;
    private boolean liked;

    public static final int ACTIVITY_ID = 453;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        configScreen();
        setContentView(R.layout.activity_level_view);

        levelNameTV = findViewById(R.id.level_name_TV);
        levelCreatorTV = findViewById(R.id.level_creator_TV);
        levelLikesTV = findViewById(R.id.level_likes_TV);

        playBTN = findViewById(R.id.play_button);

        editButton = findViewById(R.id.edit_btn_ll);
        likeButton = findViewById(R.id.like_btn_ll);
        likeBTNImage = findViewById(R.id.like_btn_image);

        likeBTNText = findViewById(R.id.like_btn_text);

        level = (Level) getIntent().getSerializableExtra("level");
        boolean edit = getIntent().getBooleanExtra("edit", false);

        if (!edit){
            editButton.setVisibility(View.GONE);
        }

        editButton.setOnClickListener(v -> {
            SoundsHandler.play("button",false);
            Intent i = new Intent(this, EditLevelActivity.class);

            i.putExtra("level", level);


            startActivityForResult(i, ACTIVITY_ID);

            // activities transitions
            overridePendingTransition(R.anim.slide_in_from_down,R.anim.slide_out_to_up);

        });

        liked = level.isLiked();

        changeLikeStatus();

        likeButton.setOnClickListener(v -> {
            SoundsHandler.play("button",false);
            NetworkRequests.getInstance(this).likeOrDislikeLevelRequest(level.getId(), response -> {

                if (liked){
                    level.addLike(-1);
                }else{
                    level.addLike(1);
                }

                levelLikesTV.setText(level.getLikes() + " likes");


                liked = !liked;

                changeLikeStatus();


            }, error -> {});
        });

        levelNameTV.setText(level.getName());
        levelCreatorTV.setText(level.getCreator());
        levelLikesTV.setText(level.getLikes() + " Likes");

        playBTN.setOnClickListener(view -> {
            SoundsHandler.play("button",false);
            LevelHandler.setLevelsSet(level);

            Intent i = new Intent(this, GameActvity.class);

            startActivity(i);

            overridePendingTransition(R.anim.slide_in_from_down,R.anim.slide_out_to_up);

        });



    }

    private void changeLikeStatus() {

        if (liked){
            likeBTNText.setText("Dislike");
            likeBTNImage.setScaleY(-1);
        }else{
            likeBTNText.setText("Like");
            likeBTNImage.setScaleY(1);


        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if(requestCode == ACTIVITY_ID && data != null){
            Log.d("rrttd", "dfgdfgdfg");

            Level level = (Level) data.getSerializableExtra("level");

            this.level = level;

        }
    }

    @Override
    public void finish() {
        super.finish();

        // creating transition animation
        overridePendingTransition(R.anim.slide_in_from_down,R.anim.slide_out_to_up);

    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        //configScreen();
    }

    private void configScreen(){

        final View decorView = getWindow().getDecorView();
        decorView.setOnSystemUiVisibilityChangeListener (new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                    decorView.setSystemUiVisibility(
                            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                }
            }
        });

        final int uiOptions =
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE;

        decorView.setSystemUiVisibility(uiOptions);



        try {
            getSupportActionBar().hide();
        }catch (Exception ignored){}
    }
}
