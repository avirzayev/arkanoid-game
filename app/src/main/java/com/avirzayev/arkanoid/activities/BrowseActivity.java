package com.avirzayev.arkanoid.activities;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.android.volley.Response;
import com.avirzayev.arkanoid.R;
import com.avirzayev.arkanoid.adapters.LevelGridViewAdapter;
import com.avirzayev.arkanoid.game.Level;
import com.avirzayev.arkanoid.game.LevelHandler;
import com.avirzayev.arkanoid.game.Levels;
import com.avirzayev.arkanoid.networking.NetworkRequests;
import com.avirzayev.arkanoid.utilities.SoundsHandler;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

public class BrowseActivity extends AppCompatActivity {

    private GridView gridView;
    private LevelGridViewAdapter adapter;
    private Button onlineLevelsBTN, myLevelsBTN, searchBTN;
    private LinkedList<Level> onlineLevels, myLevels;
    private FloatingActionButton editButton;

    private LinearLayout searchBar;

    private EditText levelSearchET;
    private Spinner spinner;

    private int mode;

    private static final String SELECTED_BUTTON = "#686868", NOT_SELECTED_BUTTON = "#8A8A8A";
    private static final int ONLINE_LEVELS = 0, MY_LEVELS = 1;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse);

        configScreen();

        adapter = new LevelGridViewAdapter(onlineLevels, this);

        gridView = findViewById(R.id.browse_grid_GV);

        editButton = findViewById(R.id.edit_float_btn);


        onlineLevelsBTN = findViewById(R.id.online_levels_BTN);
        myLevelsBTN = findViewById(R.id.my_levels_BTN);
        searchBTN = findViewById(R.id.level_search_BTN);

        spinner = findViewById(R.id.level_search_spinner);
        levelSearchET = findViewById(R.id.level_search_ET);

        searchBar = findViewById(R.id.level_search_bar_LL);




        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.search_filters, android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerAdapter);

        searchBTN.setOnClickListener(v -> {
            SoundsHandler.play("button",false);
            String text = levelSearchET.getText().toString();
            NetworkRequests.getInstance(this).pullLevelsByFilter(spinner.getSelectedItemPosition(),text, text, response -> {


                updateGrid(onlineLevels, response);

                }, error -> {
                Log.d("response", error.toString() + " debug 555");
            });
        });


        onlineLevelsBTN.setOnClickListener(view ->{
            SoundsHandler.play("button",false);
           switchToOnlineLevels();
        });

        myLevelsBTN.setOnClickListener(view ->{
            SoundsHandler.play("button",false);
            switchToMyLevels();
        });

        editButton.setOnClickListener(view ->{
            SoundsHandler.play("button",false);
            Intent i = new Intent(BrowseActivity.this, EditLevelActivity.class);

            startActivity(i);

            // activities transitions
            overridePendingTransition(R.anim.slide_in_from_up,R.anim.slide_out_to_down);

            editButton.setEnabled(false);
        });


        gridView.setOnItemClickListener((parent, view, position, id) -> {

            SoundsHandler.play("button",false);

            Intent i = new Intent(BrowseActivity.this, LevelViewActivity.class);

            i.putExtra("level", adapter.getLevels().get(position));
            i.putExtra("edit", adapter.getLevels().get(position).isEditable());

            startActivity(i);

            // activities transitions
            overridePendingTransition(R.anim.slide_in_from_up,R.anim.slide_out_to_down);

        });

        switchToOnlineLevels();

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
//        configScreen();
        editButton.setEnabled(true);
    }

    @Override
    public void finish() {
        super.finish();

        // creating transition animation
        overridePendingTransition(R.anim.slide_in_from_up,R.anim.slide_out_to_down);
    }

    private void configScreen(){

        final View decorView = getWindow().getDecorView();
        decorView.setOnSystemUiVisibilityChangeListener (new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                    decorView.setSystemUiVisibility(
                            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                }
            }
        });

        final int uiOptions =
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE;

        decorView.setSystemUiVisibility(uiOptions);



        try {
            getSupportActionBar().hide();
        }catch (Exception ignored){}
    }

    private void updateList(LinkedList<Level> levels, JSONObject response){
        levels.clear();

        try {
            JSONArray array = response.getJSONArray("levels");

            for(int i = 0; i < array.length(); i++){
                JSONObject object = array.getJSONObject(i);
                Level l = new Level(object.getInt("id"), object.getString("level_name"), object.getString("creator"), object.getInt("likes"), object.getString("level_grid"));
                l.setUserLike(object.getString("liked").equals("True"));
                l.setEditable(object.getString("editable").equals("True"));
                levels.add(l);
            }






        } catch (JSONException e) {
            Log.d("response", e.toString() + " defdsf");

        }
    }

    private void updateGrid(LinkedList<Level> levels, JSONObject response){

        updateList(levels,response);


        adapter.setLevels(levels);

        gridView.setAdapter(adapter);

    }

    @Override
    protected void onResume() {
        super.onResume();

        if(mode == ONLINE_LEVELS){
            switchToOnlineLevels();
        }else{
            switchToMyLevels();
        }


    }

    @SuppressLint("RestrictedApi")
    private void switchToOnlineLevels(){
        editButton.setVisibility(View.GONE);
        onlineLevelsBTN.setBackgroundColor(Color.parseColor(SELECTED_BUTTON));
        myLevelsBTN.setBackgroundColor(Color.parseColor(NOT_SELECTED_BUTTON));
        searchBar.setVisibility(View.VISIBLE);

        NetworkRequests.getInstance(this).pullLevelsByFilter(NetworkRequests.ArgumentKeys.filter_by_level_popularity,levelSearchET.getText().toString(), levelSearchET.getText().toString(), response -> {

            onlineLevels = new LinkedList<>();

            updateGrid(onlineLevels, response);

        }, error -> {});

        mode = ONLINE_LEVELS;




    }

    @SuppressLint("RestrictedApi")
    private void switchToMyLevels(){
        editButton.setVisibility(View.VISIBLE);
        myLevelsBTN.setBackgroundColor(Color.parseColor(SELECTED_BUTTON));
        onlineLevelsBTN.setBackgroundColor(Color.parseColor(NOT_SELECTED_BUTTON));
        searchBar.setVisibility(View.GONE);


        NetworkRequests.getInstance(this).pullLevelsOfOwnRequest(response -> {

            myLevels = new LinkedList<>();

            updateGrid(myLevels, response);

        }, error -> {});

        mode = MY_LEVELS;

    }



}
