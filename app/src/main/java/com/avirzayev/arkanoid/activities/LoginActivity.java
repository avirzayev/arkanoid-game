package com.avirzayev.arkanoid.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.avirzayev.arkanoid.R;
import com.avirzayev.arkanoid.networking.NetworkRequests;
import com.avirzayev.arkanoid.utilities.SoundsHandler;

import org.json.JSONException;


public class LoginActivity extends AppCompatActivity {

    private Button loginBTN, noAccountBTN;
    private EditText usernameET, emailET, passwordET;
    private TextView titleTV, emailErrorTV, usernameErrorTV, passwordErrorTV;
    private ProgressBar progressBar;

    private static final int LOGIN = 0, REGISTER = 1;
    private int mode = 0; // 0 - login, 1 - register

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        configScreen();

        loginBTN = findViewById(R.id.login_BTN);
        noAccountBTN = findViewById(R.id.no_account_BTN);

        usernameET = findViewById(R.id.username_register_ET);
        emailET = findViewById(R.id.email_login_ET);
        passwordET = findViewById(R.id.password_login_ET);

        titleTV = findViewById(R.id.login_title_TV);

        usernameErrorTV = findViewById(R.id.username_error_TV);
        emailErrorTV = findViewById(R.id.email_error_TV);
        passwordErrorTV = findViewById(R.id.password_error_TV);

        progressBar = findViewById(R.id.login_progressbar_PB);

        // setting a listener to switch between login and register
        noAccountBTN.setOnClickListener(view -> {
            switchMode(mode == LOGIN?REGISTER:LOGIN);
            SoundsHandler.play("button",false);
        });

        loginBTN.setOnClickListener(view -> {
            SoundsHandler.play("button",false);
            String username = usernameET.getText().toString();
            String email = emailET.getText().toString();
            String password = passwordET.getText().toString();


            setErrorVisibility(View.GONE);


            if (mode == LOGIN){

                if(!isEmailValid(email)){
                    emailErrorTV.setText("This email does not valid");
                    emailErrorTV.setVisibility(View.VISIBLE);

                    return;

                } if(password.length() <= 0 ){
                    passwordErrorTV.setText("You must enter a password");
                    passwordErrorTV.setVisibility(View.VISIBLE);

                    return;
                }

                toggleLoad(true);
                NetworkRequests.getInstance(this).loginRequest(email, password, response -> {
                    try {

                        int code = response.getInt(NetworkRequests.ArgumentKeys.STATUS_KEY);

                        Log.d("response", response.toString());

                        if(code == NetworkRequests.RequestStatus.SUCCESS){
                            finish();
                        }

                        if (code == NetworkRequests.RequestStatus.EMAIL_NOT_EXISTS){
                            emailErrorTV.setText("This email does not exist");
                            emailErrorTV.setVisibility(View.VISIBLE);
                        }

                        if (code == NetworkRequests.RequestStatus.WRONG_PASSWORD){
                            passwordErrorTV.setText("The password is wrong");
                            passwordErrorTV.setVisibility(View.VISIBLE);
                        }

                        if (code == NetworkRequests.RequestStatus.ALREADY_LOGGED){
                            finish();
                        }

                        toggleLoad(false);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
                    toggleLoad(false);
                    Toast.makeText(this, "An Error has been occurred", Toast.LENGTH_SHORT).show();

                });
            }

            else if(mode == REGISTER){

                if(!isEmailValid(email)){
                    emailErrorTV.setText("This email does not valid");
                    emailErrorTV.setVisibility(View.VISIBLE);

                    return;

                } if(password.length() < 6){
                    passwordErrorTV.setText("Your password is too short");
                    passwordErrorTV.setVisibility(View.VISIBLE);

                    return;
                }
                if (username.length() < 3){
                    usernameErrorTV.setText("Your username is too short");
                    usernameErrorTV.setVisibility(View.VISIBLE);

                    return;
                }

                toggleLoad(true);
                NetworkRequests.getInstance(this).registerRequest(email, username,password, response -> {
                    try {
                        int code = response.getInt(NetworkRequests.ArgumentKeys.STATUS_KEY);

                        Log.d("response", response.toString());

                        if(code == NetworkRequests.RequestStatus.SUCCESS){
                            finish();
                        }

                        if (code == NetworkRequests.RequestStatus.EMAIL_TAKEN){
                            emailErrorTV.setText("This email is taken");
                            emailErrorTV.setVisibility(View.VISIBLE);
                        }

                        if (code == NetworkRequests.RequestStatus.USERNAME_TAKEN){
                            usernameErrorTV.setText("This username is taken");
                            usernameErrorTV.setVisibility(View.VISIBLE);
                        }

                        if (code == NetworkRequests.RequestStatus.ALREADY_LOGGED){
                            finish();
                        }

                        toggleLoad(false);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }



                }, error -> {
                    toggleLoad(false);
                    Toast.makeText(this, "An Error has been occurred", Toast.LENGTH_SHORT).show();
                });
            }
        });

        switchMode(LOGIN);
    }

    // switches the mode between login and register
    private void switchMode(int mode){
        if(mode == LOGIN){

            this.mode = mode; // updating the mode

            // setting the views
            loginBTN.setText("Login");
            noAccountBTN.setText("I Don't Have an Account");
            usernameET.setVisibility(View.GONE);
            titleTV.setText("Log In");


        }else if(mode == REGISTER){

            this.mode = mode; //updating the mode

            // setting the views
            loginBTN.setText("Register");
            noAccountBTN.setText("I already have an Account");

            usernameET.setVisibility(View.VISIBLE);

            titleTV.setText("Register");

        }
    }

    @Override
    public void finish() {
        super.finish();

        // creating transition animation
        overridePendingTransition(R.anim.slide_in_from_up,R.anim.slide_out_to_down);
    }

    private void toggleLoad(boolean toggle){
        if(toggle){
            progressBar.setVisibility(View.VISIBLE);
            loginBTN.setVisibility(View.GONE);
        }else{
            progressBar.setVisibility(View.GONE);
            loginBTN.setVisibility(View.VISIBLE);
        }

        setViewStatus(!toggle);
    }

    //disables or enables all the views
    private void setViewStatus(boolean value){
        usernameET.setEnabled(value);
        emailET.setEnabled(value);
        passwordET.setEnabled(value);
        loginBTN.setEnabled(value);
        noAccountBTN.setEnabled(value);
    }

    private void configScreen(){

        final View decorView = getWindow().getDecorView();
        decorView.setOnSystemUiVisibilityChangeListener (new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                    decorView.setSystemUiVisibility(
                            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                }
            }
        });

        final int uiOptions =
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE;

        decorView.setSystemUiVisibility(uiOptions);



        try {
            getSupportActionBar().hide();
        }catch (Exception ignored){}
    }

    private static boolean isEmailValid(String email) {
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return email.matches(regex);
    }

    private void setErrorVisibility(int value){
        emailErrorTV.setVisibility(value);
        usernameErrorTV.setVisibility(value);
        passwordErrorTV.setVisibility(value);
    }
}
