package com.avirzayev.arkanoid.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.avirzayev.arkanoid.R;
import com.avirzayev.arkanoid.game.Level;
import com.avirzayev.arkanoid.game.LevelHandler;
import com.avirzayev.arkanoid.game.Levels;
import com.avirzayev.arkanoid.networking.NetworkRequests;
import com.avirzayev.arkanoid.utilities.SoundsHandler;

import org.json.JSONException;

import java.util.Random;

// Main screen activity
public class MainActivity extends AppCompatActivity implements LifecycleObserver {

    private Button playBTN, exitBTN, loginBTN, browseBTN; // buttons

    private ImageView animationIMG; // moving cloud image for animation
    private Switch soundSW;
    private TextView highscoreTV;

    public static boolean inGame = false;

    public static boolean isUserAuthenticated = false;

    private AnimatorListenerAdapter adapter; // animation adapter
    private LinearLayout animationBounds; // a linear layout that indicates the moving cloud bounds

    private boolean isAnimationStarted; // indicates if the animation already started
    private SharedPreferences sharedPreferences; //sp
    private int currentHighScore; // current high score

    public static final int ACTIVITY_ID = 111; // random id of the activity

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        // initializing the sounds
        SoundsHandler.init(this);


        // configuring the screen
        configScreen();

        // getting the buttons view
        playBTN = findViewById(R.id.play_button);
        exitBTN = findViewById(R.id.exit_button);
        loginBTN = findViewById(R.id.login_pointer_BTN);
        browseBTN = findViewById(R.id.browse_BTN);

        browseBTN.setOnClickListener(view ->{
            SoundsHandler.play("button",false);
            Intent i = new Intent(MainActivity.this, BrowseActivity.class);

            startActivity(i);

            // activities transitions
            overridePendingTransition(R.anim.slide_in_from_down,R.anim.slide_out_to_up);
        });

        loginBTN.setOnClickListener(view -> {
            SoundsHandler.play("button",false);
            if (isUserAuthenticated){
                NetworkRequests.getInstance(this).clearCookie();
                isUserAuthenticated = false;
                loginBTN.setText("Log in");
                browseBTN.setVisibility(View.GONE);

                return;
            }

            Intent i = new Intent(MainActivity.this, LoginActivity.class);

            startActivity(i);

            // activities transitions
            overridePendingTransition(R.anim.slide_in_from_down,R.anim.slide_out_to_up);
        });


        // setting listeners for the buttons
        playBTN.setOnClickListener(view -> {
            SoundsHandler.play("button",false);
            LevelHandler.setLevelsSet(Levels.getLevels());
            Intent i = new Intent(MainActivity.this, GameActvity.class);

            // starting activity and waiting for the result of the high score
            startActivityForResult(i,ACTIVITY_ID);

            // activities transitions
            overridePendingTransition(R.anim.slide_in_from_down,R.anim.slide_out_to_up);


        });
        exitBTN.setOnClickListener(view -> {SoundsHandler.play("button",false); finish();  });

        // getting image and the bounds of the animation
        this.animationIMG = findViewById(R.id.animation);

        animationBounds = findViewById(R.id.animation_bounds);

        // creating the animation
        adapter = new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {

                // minimum height
                int min = (int) animationBounds.getY();

                // maximum height
                int max= (int) (animationBounds.getY() + animationBounds.getHeight() - animationIMG.getHeight());

                // setting the cloud height to random value in bounds
                animationIMG.setY(new Random().nextInt(max - min) + min);

                // flipping the image (this run periodically and we always want to change the direction)
                animationIMG.setScaleX(-animationIMG.getScaleX());

                // creating moving animation and setting it to move the opposite direction
                animation = ObjectAnimator.ofFloat(animationIMG, "translationX", -(animationIMG.getX()));

                // the animation will move 8 seconds
                animation.setDuration(8000);

                // adding this adapter to the animation and stating the animation
                animation.addListener(adapter);
                animation.start();
            }
        };

        // creating shared preferences
        sharedPreferences = getSharedPreferences("data", MODE_PRIVATE);

        // getting the current high score
        currentHighScore = sharedPreferences.getInt("high_score", 0);

        // updating the score in the screen
        highscoreTV = findViewById(R.id.highscoreTV);
        highscoreTV.setText("High Score: " + currentHighScore);

        // sound switch
        soundSW = findViewById(R.id.soundsSW);

        // setting the settings according to the current switch status saved in sp
        SoundsHandler.setSound(sharedPreferences.getBoolean("sounds",false));

        // checking the switch
        soundSW.setChecked(SoundsHandler.hasSound());

        // updating sound settings each time the switch is toggled
        soundSW.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean("sounds", b);
                editor.apply();
                SoundsHandler.setSound(b);

                SoundsHandler.play("back_music",true);

            }
        });

        // playing background music
        SoundsHandler.play("back_music",true);

        checkUserAuthentication();

        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);


    }


    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        //configScreen();

        // starting the animation
        if (hasFocus && !isAnimationStarted) {

            isAnimationStarted = true;

            animationIMG.setX(this.animationIMG.getX() + this.animationIMG.getWidth()*2);

            int min = (int) animationBounds.getY();
            int max= (int) (animationBounds.getY() + animationBounds.getHeight() - animationIMG.getHeight());

            animationIMG.setY(new Random().nextInt(max - min) + min);


            // the actual animation
            ObjectAnimator animation = ObjectAnimator.ofFloat(this.animationIMG, "translationX", -(this.animationIMG.getX()));
            animation.setDuration(8000);
            animation.start();

            animation.addListener(adapter);
        }


    }

    // when the game ends
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);



        // if the code matches to the activity -> update the high score if needed
        if(requestCode == ACTIVITY_ID && data != null){

            currentHighScore = Math.max(currentHighScore, data.getIntExtra("score",0));

            SharedPreferences.Editor editor = sharedPreferences.edit();

            editor.putInt("high_score", currentHighScore);
            editor.apply();

            highscoreTV.setText("High Score: " + currentHighScore);

            SoundsHandler.play("back_music", true);



        }
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onResume() {
        super.onResume();
        checkUserAuthentication();

    }

    private void configScreen(){

        final View decorView = getWindow().getDecorView();
        decorView.setOnSystemUiVisibilityChangeListener (new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                    decorView.setSystemUiVisibility(
                            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                }
            }
        });

        final int uiOptions =
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE;

        decorView.setSystemUiVisibility(uiOptions);



        try {
            getSupportActionBar().hide();
        }catch (Exception ignored){}
    }



    private void checkUserAuthentication(){
        NetworkRequests.getInstance(this).checkAuthenticationRequest(response -> {
            try {
                int code = response.getInt(NetworkRequests.ArgumentKeys.STATUS_KEY);

                isUserAuthenticated = code == NetworkRequests.RequestStatus.SUCCESS;

                if (isUserAuthenticated){
                    loginBTN.setText("Log out");
                    browseBTN.setVisibility(View.VISIBLE);
                }else{
                    loginBTN.setText("Log in");
                    browseBTN.setVisibility(View.GONE);
                }

            } catch (JSONException e) {
               Log.d("response", "err");
            }

        }, error -> {
            isUserAuthenticated = false;
            Log.d("response", error.toString());

        });
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        if (!inGame){
            SoundsHandler.pause("back_music");
        }


    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {

        if (!inGame){
            SoundsHandler.resume("back_music");

        }


    }

}

