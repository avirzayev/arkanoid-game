package com.avirzayev.arkanoid.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import com.avirzayev.arkanoid.R;
import com.avirzayev.arkanoid.dialogs.PauseDialog;
import com.avirzayev.arkanoid.game.GameView;
import com.avirzayev.arkanoid.utilities.SoundsHandler;

public class GameActvity extends AppCompatActivity {

    // custom view for graphics
    private GameView gameView;

    // indicates if the game is already paused somewhere else
    private boolean wherePaused;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SoundsHandler.stopAll();
        SoundsHandler.play("ingame_music", true);

        MainActivity.inGame = true;


        // creating and setting the view
        gameView = new GameView(this);
        setContentView(gameView);

        // disabling the actionbar and making full screen
        configScreen();



    }

    // configuring the screen again if something changes the focus
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        configScreen();
    }

    // if the activity ends
    @Override
    public void finish() {
        super.finish();

        // ending the game
        gameView.endGame();

        // creating transition animation
        overridePendingTransition(R.anim.slide_in_from_up,R.anim.slide_out_to_down);

        SoundsHandler.stopAll();

        MainActivity.inGame = false;


    }

    // button handling
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        // if the back key is pressed and the game isn't paused -> pause the game
        if (keyCode == KeyEvent.KEYCODE_BACK && !gameView.isPause()) {
            gameView.pauseGame();
            return false;
        }
        return false;
    }

    // when the activity is paused
    @Override
    protected void onPause() {
        super.onPause();

        // update if the game were paused
        wherePaused = gameView.isPause();

        // pause the game
        gameView.setPause(true);

        // pause the music
        SoundsHandler.pause("ingame_music");

    }

    // when the activity continues
    @Override
    protected void onResume() {
        super.onResume();
        // if the game was paused only once -> show the dialog
        if (gameView.isPause() && !wherePaused){
            new PauseDialog(this, false).show();
        }

        // resetting the indicator
        wherePaused = false;

        // resuming the music
        SoundsHandler.resume("ingame_music");

    }

    private void configScreen(){

        final View decorView = getWindow().getDecorView();
        decorView.setOnSystemUiVisibilityChangeListener (new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                    decorView.setSystemUiVisibility(
                            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                }
            }
        });

        final int uiOptions =
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE;

        decorView.setSystemUiVisibility(uiOptions);



        try {
            getSupportActionBar().hide();
        }catch (Exception ignored){}
    }
}
