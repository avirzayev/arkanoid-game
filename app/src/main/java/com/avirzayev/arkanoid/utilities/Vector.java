package com.avirzayev.arkanoid.utilities;

/**
    Class that defines a position in the screen, it also handles the velocity calculations and directions measurements
**/
public class Vector {

    private float posX, posY; // coordinates
    private float velX, velY; // velocities
    private float accX, accY; // acceleration (Potential)


    /* ************
     constructors
    *********** */

    public Vector(float posX, float posY, float velX, float velY, float accX, float accY) {
        this.posX = posX;
        this.posY = posY;
        this.velX = velX;
        this.velY = velY;
        this.accX = accX;
        this.accY = accY;
    }

    public Vector(float posX, float posY, float velX, float velY) {
        this(posX,posY,velX,velY,0,0);
    }

    public Vector(float posX, float posY) {
        this(posX,posY,0,0,0,0);
    }

    public Vector(Vector v){
        this(v.posX,v.posY,v.velX,v.velY,v.accX,v.accY);
    }

    /* ************
     end of constructors
    *********** */

    /**
     *  updates the vector position by adding velocities and acceleration
     * **/

    public void update(float delta){

        velX += accX * delta;
        velY += accY * delta;

        posX += velX * delta;
        posY += velY * delta;

    }

    /** returns if there is slope **/
    public boolean isSlopeAvalible(){
        return velY != 0;
    }

    /** returns the current slope **/
    public float getSlope(){
        return velX/velY;
    }
    /**
     *  resets the vectors of velocity and acceleration
     * **/

    public void resetMovement(){
        velX = velY = accY = accX = 0;
    }


    /**
     *  adds value to current x position
     * **/

    public void addX(float x){
        posX += x;
    }

    /**
     *  adds value to current y position
     * **/
    public void addY(float y){
        posY += y;
    }


    /* ************
     getters and setters
     *********** */

    public float getPosX() {
        return posX;
    }

    public void setPosX(float posX) {
        this.posX = posX;
    }

    public float getPosY() {
        return posY;
    }

    public void setPosY(float posY) {
        this.posY = posY;
    }

    public float getVelX() {
        return velX;
    }

    public void setVelX(float velX) {
        this.velX = velX;
    }

    public float getVelY() {
        return velY;
    }

    public void setVelY(float velY) {
        this.velY = velY;
    }

    public float getAccX() {
        return accX;
    }

    public void setAccX(float accX) {
        this.accX = accX;
    }

    public float getAccY() {
        return accY;
    }

    public void setAccY(float accY) {
        this.accY = accY;
    }

    /* ************
     end of getters and setters
     *********** */
}
