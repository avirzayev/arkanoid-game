package com.avirzayev.arkanoid.utilities;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;

import com.avirzayev.arkanoid.R;

import java.util.HashMap;

// simple collection for image to simplify the access
public class ImageHandler {

    private static HashMap<String, Bitmap> images;


    public static void loadImages(Context context){

        if (images != null) return;

        images = new HashMap<>();

        images.put("headsup", BitmapFactory.decodeResource(context.getResources(), R.drawable.test));
        images.put("brick", BitmapFactory.decodeResource(context.getResources(), R.drawable.brick));
        images.put("wall", BitmapFactory.decodeResource(context.getResources(), R.drawable.wall2));

        images.put("menuback", BitmapFactory.decodeResource(context.getResources(), R.drawable.mainmenuback));

        images.put("unbreak_brick", BitmapFactory.decodeResource(context.getResources(), R.drawable.unbreak_brick));
        images.put("hard_brick", BitmapFactory.decodeResource(context.getResources(), R.drawable.hard_brick));

        images.put("panel", BitmapFactory.decodeResource(context.getResources(), R.drawable.panel));

        images.put("pause", BitmapFactory.decodeResource(context.getResources(), R.drawable.pause));



    }

    public static Bitmap darkenBitMap(Bitmap bm, int alpha) {
        Canvas canvas = new Canvas(bm);
        canvas.drawARGB(alpha,0,0,0);
        canvas.drawBitmap(bm, new Matrix(), new Paint());
        return bm;
    }

    public static Bitmap getImage(String key){
        return images.get(key);
    }
}
