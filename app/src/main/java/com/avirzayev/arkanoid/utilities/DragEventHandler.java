package com.avirzayev.arkanoid.utilities;

import com.avirzayev.arkanoid.game.GameView;
import com.avirzayev.arkanoid.gameobjects.GameObject;

/**
 * Handles drag events on a certain game object
 * **/

public class DragEventHandler {

    private static float mouseXoffset; // offset from objects pos x
    private static float mouseYoffset; // offset from object pos y

    private static GameObject object; // target drag object
    private static Vector tempVec; // temporary vector of the target object to keep velocity

    private static boolean drag; // drag flag

    private static DragAxis axis; // in which axis the drag affect

    public enum DragAxis{
        X_AXIS, Y_AXIS, BOTH;
    }


    /** sets the target object for drag and in which axis **/
    public static void requestEventFor(GameObject obj, DragAxis a){
        object = obj;
        axis = a;
    }

    /** clears the object target from the event **/
    public static void clearEvent(){
        object = null;
        tempVec = null;
        drag = false;
    }

    /** enables the drag when there is a intersection with the mouse, runs perpetually **/
    public static void enableDrag(float x, float y){

        if(object == null || !object.intersects(x,y)) return;

        drag = true;

        if(object != null){
            tempVec = new Vector(object.getVector());
            object.getVector().resetMovement();

            mouseXoffset = x - object.getVector().getPosX();
            mouseYoffset = y - object.getVector().getPosY();
        }



    }

    /** disables the drag when the is no intersection, runs perpetually **/
    public static void disableDrag(){
        drag = false;
        if(object != null && tempVec != null){
            object.getVector().setVelX(tempVec.getVelX());
            object.getVector().setVelY(tempVec.getVelY());
            object.getVector().setAccX(tempVec.getAccX());
            object.getVector().setAccY(tempVec.getAccY());
        }
    }

    /** updates the coords of the drag according to the mouse, runs perpetually **/
    public static void updateDragCoords(float x, float y){

        if(!drag || object == null) return;

        switch (axis){
            case BOTH:
                checkAndAddX(x);
                checkAndAddY(y);
                break;
            case X_AXIS:
                checkAndAddX(x);
                break;
            case Y_AXIS:
                checkAndAddY(y);
                break;
        }

    }

    public static void checkAndAddX(float x){

        if(x - mouseXoffset + object.getWidth() > GameView.WIDTH || x - mouseXoffset < 0) return;

        object.getVector().setPosX(x - mouseXoffset);
    }

    public static void checkAndAddY(float y){

        if(y - mouseYoffset + object.getHeight() > GameView.HEIGHT || y - mouseYoffset < 0) return;

        object.getVector().setPosY(y - mouseXoffset);
    }






}
