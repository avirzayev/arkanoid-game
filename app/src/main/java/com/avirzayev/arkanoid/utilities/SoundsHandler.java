package com.avirzayev.arkanoid.utilities;

import android.content.Context;
import android.media.MediaPlayer;

import com.avirzayev.arkanoid.R;

import java.util.HashMap;

// handles all the sounds, simplify the access to sound
public class SoundsHandler {

    private static boolean sound; // if the game muted or not
    private static HashMap<String, MediaPlayer> sounds;

    public static void init(Context context){

        sounds = new HashMap<>();

        sounds.put("ball", MediaPlayer.create(context, R.raw.ball));
        sounds.put("back_music", MediaPlayer.create(context, R.raw.background_music));
        sounds.put("ingame_music", MediaPlayer.create(context, R.raw.ingame_music));
        sounds.put("button", MediaPlayer.create(context, R.raw.button));
    }

    public static void play(String key ,boolean loop){
        if (!sound) return;

        if(sounds.get(key).isPlaying() && !key.equals("ball")) return;

        stop(key);

        sounds.get(key).setLooping(loop);
        sounds.get(key).start();
    }

    public static void stop(String key){
        if(sounds.get(key).isPlaying()){
            sounds.get(key).pause();
            sounds.get(key).seekTo(0);
        }
    }

    public static void stopAll(){
        for (String key:
             sounds.keySet()) {
            stop(key);
        }
    }

    public static void pause(String key){
        sounds.get(key).pause();

    }

    public static void resume(String key){
        if (!sound) return;

        sounds.get(key).start();

    }

    public static boolean hasSound() {
        return sound;
    }

    public static void setSound(boolean sound) {
        SoundsHandler.sound = sound;

        if (!SoundsHandler.sound){
            stopAll();
        }
    }
}
