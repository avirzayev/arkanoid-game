package com.avirzayev.arkanoid.utilities;

import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;

import com.avirzayev.arkanoid.gameobjects.GameObject;

// image that works like a button
public class DrawableButton extends GameObject {

    private View.OnClickListener onClickListener;
    public DrawableButton(Vector vector, float width, float height, Bitmap bitmap) {
        super(vector, width, height, bitmap);
    }

    public void updateClick(float x, float y){
        if(intersects(x,y)){
            Log.d("pos", "boop");
            SoundsHandler.play("button",false);
            onClickListener.onClick(null);
        }
    }

    public View.OnClickListener getOnClickListener() {
        return onClickListener;
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }


}
