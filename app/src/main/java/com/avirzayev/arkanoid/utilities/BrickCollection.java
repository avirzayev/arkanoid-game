package com.avirzayev.arkanoid.utilities;

import android.graphics.Canvas;

import com.avirzayev.arkanoid.game.GameView;
import com.avirzayev.arkanoid.game.LevelHandler;
import com.avirzayev.arkanoid.gameobjects.Ball;
import com.avirzayev.arkanoid.gameobjects.Brick;
import com.avirzayev.arkanoid.gameobjects.HardBrick;
import com.avirzayev.arkanoid.gameobjects.UnbreakableBrick;

import java.util.ArrayList;

// handles the bricks
public class BrickCollection {

    private ArrayList<Brick> bricks; // brick collection
    private int breakableSize; // how much bricks can be break

    public BrickCollection() {
        this.bricks = new ArrayList<>();
    }


    public void update(float delta, Ball ball) {

        boolean collisionHappened = false; // if there was a collision
        Brick remove = null; // brick that needs to be removed

        for (Brick brick :
                bricks) {
            brick.update(delta);


            // if there is a collision and no collision happened before - avoiding the ball go though balls
            if( ball.checkCollision(brick) && !collisionHappened){
                SoundsHandler.play("ball",false);

                collisionHappened = true;

                if(!brick.isBreakable()) return;

                brick.setHealth(brick.getHealth()-1);

                if (brick.getHealth() > 0){

                    // making the brick more darker
                    brick.setBitmap(ImageHandler.darkenBitMap(brick.getBitmap(), 200/brick.getHealth()));
                    continue;
                }

                remove = brick;
                LevelHandler.increaseBrickHitScore();
            }


        }

        if(remove != null) removeBrick(remove);
    }

    public void draw(Canvas canvas) {
        for (Brick b :
                bricks) {
            b.draw(canvas);
        }

    }

    public void addBrick(Brick brick){
        bricks.add(brick);
        if(brick.isBreakable()){
            breakableSize++;
        }
    }

    public void removeBrick(Brick brick){
        bricks.remove(brick);
        if(brick.isBreakable()){
            breakableSize--;
        }
    }

    public void clear(){
        bricks.clear();
    }

    public ArrayList<Brick> getBricks() {
        return bricks;
    }


    // generating a brick collection
    public static BrickCollection generateBricks(int[][] level){

        BrickCollection brickCollection = new BrickCollection(); // empty collection

        float brickWidth = Brick.BRICK_WIDTH + Brick.PADDING;
        float brickHeight = Brick.BRICK_HEIGHT + Brick.PADDING;


        float startX = (GameView.WIDTH - (level[0].length*brickWidth - Brick.PADDING))/2;

        float startY = HeadUpDisplay.UPPER_DIMENSIONS_HEIGHT + Brick.PADDING;


        float saveStartX = startX;


        for(int r = 0; r < level.length; r++){
            for(int c = 0; c  < level[r].length; c++){
                if(level[r][c] == 1){
                    brickCollection.addBrick(new Brick(startX, startY));
                }else if(level[r][c] == 2){
                    brickCollection.addBrick(new UnbreakableBrick(startX, startY));
                }
                else if(level[r][c] == 3){
                    brickCollection.addBrick(new HardBrick(startX, startY));
                }
                startX += brickWidth;
            }
            startX  = saveStartX;
            startY += brickHeight;
        }

        return brickCollection;

    }

    public int getBreakableSize() {
        return breakableSize;
    }
}
