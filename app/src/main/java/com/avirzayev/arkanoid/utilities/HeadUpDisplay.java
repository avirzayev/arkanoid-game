package com.avirzayev.arkanoid.utilities;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.avirzayev.arkanoid.game.LevelHandler;
import com.avirzayev.arkanoid.gameobjects.Ball;
import com.avirzayev.arkanoid.gameobjects.GameObject;

// shows all the stuff about the game, like score and balls
public class HeadUpDisplay extends GameObject {


    // consists of 2 parts, upper space and lower space, spaces dimensions:
    public static float UPPER_DIMENSIONS_X = 0;
    public static float UPPER_DIMENSIONS_Y = 0;

    public static float LOWER_DIMENSIONS_X = 0;
    public static float LOWER_DIMENSIONS_Y = 0;

    public static float UPPER_DIMENSIONS_WIDTH = 0;
    public static float UPPER_DIMENSIONS_HEIGHT = 0;

    public static float LOWER_DIMENSIONS_WIDTH = 0;
    public static float LOWER_DIMENSIONS_HEIGHT = 0;


    public HeadUpDisplay(float x, float y, float width, float height) {
        super(x, y, width, height, Bitmap.createScaledBitmap(ImageHandler.getImage("headsup"), (int)UPPER_DIMENSIONS_WIDTH,(int)UPPER_DIMENSIONS_HEIGHT, false));

    }

    @Override
    public void draw(Canvas canvas) {

        // font size
        float fontSize = UPPER_DIMENSIONS_HEIGHT/2.5f;

        // drawing the image in the space
        canvas.drawBitmap(bitmap,vector.getPosX(), vector.getPosY() , null);
        canvas.drawBitmap(bitmap,vector.getPosX(), vector.getPosY() + LOWER_DIMENSIONS_Y, null);

        // creating the texts and and drawing them
        Text level = new Text("Level: " + LevelHandler.getLevel(), UPPER_DIMENSIONS_X + UPPER_DIMENSIONS_WIDTH/30f,0, fontSize, Color.WHITE);

        level.centerToHeight(vector.getPosY(), UPPER_DIMENSIONS_HEIGHT);

        level.drawText(canvas);

        Text score = new Text("Score: " + LevelHandler.getScore(),  UPPER_DIMENSIONS_WIDTH - UPPER_DIMENSIONS_WIDTH/30f, 0, fontSize, Paint.Align.RIGHT, Color.WHITE);

        score.centerToHeight(vector.getPosY(), UPPER_DIMENSIONS_HEIGHT);

        score.drawText(canvas);

        // end of text creation

        // drawing row of balls which defines the lives
        float startBallX = LOWER_DIMENSIONS_WIDTH/20 + LOWER_DIMENSIONS_X;

        float startBallY = LOWER_DIMENSIONS_HEIGHT/2 + LOWER_DIMENSIONS_Y + vector.getPosY();

        Paint ball = new Paint();
        ball.setColor(Color.WHITE);

        for(int i =0; i < LevelHandler.getBallsLeft(); i++){
            canvas.drawCircle(startBallX, startBallY, Ball.RADIUS,ball);
            startBallX += Ball.RADIUS*2.3;
        }



    }
}
