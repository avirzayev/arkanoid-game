package com.avirzayev.arkanoid.utilities;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

// simpler way to handle text creation and rendering
public class Text {

    private Rect rect;

    private String text;

    private Paint paint;

    private float x, y;

    private int color;

    public Text(String text, float x, float y, float fontSize, Paint.Align align, int color) {
        this(text, x, y, fontSize,color);

        paint.setTextAlign(align);
        refresh();

    }

    public Text(String text, float x, float y, float fontSize, int color) {
        this.text = text;

        paint = new Paint();

        paint.setColor(color);

        paint.setTextSize(fontSize);

        this.x = x;
        this.y = y;

        rect = new Rect();


        refresh();
    }

    public void refresh(){
        paint.getTextBounds(text,0, text.length(), rect);

    }

    public void drawText(Canvas canvas){
        canvas.drawText(text, x,y, paint);
    }

    public void centerToWidth(float x, float width){
        this.x = x + (width - rect.width())/2;
    }

    public void centerToHeight(float y, float height){
        this.y = y + (height - rect.height())/2 + rect.height();
    }

    public Rect getRect() {
        return rect;
    }

    public void setRect(Rect rect) {
        this.rect = rect;
    }

    public String getText() {
        refresh();
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Paint getPaint() {
        return paint;
    }

    public void setPaint(Paint paint) {
        refresh();
        this.paint = paint;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public int getColor() {
        return paint.getColor();
    }

    public void setColor(int color) {
        paint.setColor(color);
    }
}
