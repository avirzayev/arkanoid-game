package com.avirzayev.arkanoid.utilities;

import android.graphics.Canvas;
import android.graphics.Paint;

public class TextButton extends DrawableButton {

    private int backColor;
    private Text text;
    public static final int padding = 30;

    public TextButton(Text text, int backColor) {
        super(new Vector(text.getX() - padding, text.getY() - text.getRect().height() - padding), text.getRect().width() + padding, text.getRect().height() + padding, null);

        this.backColor = backColor;
        this.text = text;



    }

    @Override
    public void draw(Canvas canvas) {
        Paint p = new Paint();
        p.setColor(backColor);

        canvas.drawRect(vector.getPosX(), vector.getPosY(),  text.getPaint().measureText(text.getText())+ text.getX() + padding, text.getY() + padding, p);
        text.drawText(canvas);
    }
}
