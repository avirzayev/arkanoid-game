package com.avirzayev.arkanoid.dialogs;


import android.content.Context;
import android.view.View;


import com.avirzayev.arkanoid.R;
import com.avirzayev.arkanoid.game.LevelHandler;

public class LevelCompletedDialog extends GameOverDialog{

    public LevelCompletedDialog(Context context) {
        super(context);

        if(LevelHandler.levelsSet.size() > 1){
            title.setText("Levels Finished!");
        }else{
            title.setText("Level Finished!");
        }


        subtitle.setVisibility(View.VISIBLE);

    }


}
