package com.avirzayev.arkanoid.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.avirzayev.arkanoid.R;
import com.avirzayev.arkanoid.activities.MainActivity;
import com.avirzayev.arkanoid.game.GameView;
import com.avirzayev.arkanoid.game.LevelHandler;
import com.avirzayev.arkanoid.utilities.SoundsHandler;

// game over dialog
public class GameOverDialog extends Dialog {

    protected Button exit, play; // buttons
    protected TextView title, subtitle; // title

    public GameOverDialog(Context context) {
        super(context);
        setContentView(R.layout.gameover_dialog);

        // setting animation to the dialog
        getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

        // getting the view
        play = findViewById(R.id.gameover_dialog_play_button);
        exit = findViewById(R.id.gameover_dialog_exit_button);
        title = findViewById(R.id.title_dialog);
        subtitle = findViewById(R.id.subtitle_dialog);

        // play again - initialize game
        play.setOnClickListener(view -> {SoundsHandler.play("button",false);GameView.gameView.initializeGame();dismiss();  });


        // exit -> finish activity and return the score
        exit.setOnClickListener(view -> {
            SoundsHandler.play("button",false);

            dismiss();
            ((Activity)context).setResult(MainActivity.ACTIVITY_ID, new Intent().putExtra("score",LevelHandler.getScore()));
            ((Activity)context).finish();

        });


        // setting the dialog fit the screen width
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(lp);


        setCancelable(false);

    }

    @Override
    public void show() {
        // disabling dark background of the dialog
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);

        super.show();
    }

    @Override
    public void dismiss() {
        super.dismiss();

        getWindow().getDecorView().setSystemUiVisibility(
                ((Activity)GameView.gameView.getContext()).getWindow().getDecorView().getSystemUiVisibility());

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
    }
}
