package com.avirzayev.arkanoid.dialogs;

import android.content.Context;
import android.view.View;

import com.avirzayev.arkanoid.R;
import com.avirzayev.arkanoid.game.GameView;
import com.avirzayev.arkanoid.utilities.SoundsHandler;

// game pause dialog is looks the same like gameOver
public class PauseDialog extends GameOverDialog {
    public PauseDialog(Context context, boolean inGame) {
        super(context);
        if(!inGame) getWindow().setWindowAnimations(R.style.DialogNoAnimation);


        // changing the text
        title.setText("PAUSED");
        play.setText("Resume");

        // changing the play listener to continue the game
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GameView.gameView.setPause(false);
                SoundsHandler.play("button",false);
                dismiss();
            }
        });
    }
}
