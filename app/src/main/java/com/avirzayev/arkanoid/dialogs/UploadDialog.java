package com.avirzayev.arkanoid.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.android.volley.toolbox.JsonObjectRequest;
import com.avirzayev.arkanoid.R;
import com.avirzayev.arkanoid.activities.LevelViewActivity;
import com.avirzayev.arkanoid.activities.MainActivity;
import com.avirzayev.arkanoid.game.Level;
import com.avirzayev.arkanoid.game.LevelHandler;
import com.avirzayev.arkanoid.networking.NetworkRequests;

public class UploadDialog extends Dialog {

    private Button cancelBTN, uploadBTN;
    private ProgressBar progressBar;
    private EditText levelNameET;
    private TextView uploadTitle;

    public UploadDialog(@NonNull Context context, int[][] level, boolean isUploading, Level levelObj) {
        super(context);

        setContentView(R.layout.upload_dialog);

        cancelBTN = findViewById(R.id.upload_cancel_BTN);
        uploadBTN = findViewById(R.id.upload_level_BTN);
        progressBar = findViewById(R.id.progressbar_upload_PB);
        levelNameET = findViewById(R.id.upload_level_name_ET);
        uploadTitle = findViewById(R.id.upload_title_TV);

        if (!isUploading){
            uploadBTN.setText("Save");
            levelNameET.setVisibility(View.GONE);
            uploadTitle.setText("Save");
        }

        uploadBTN.setOnClickListener(v -> {

            if(!isUploading){
                NetworkRequests.getInstance(context).overrideLevelRequest(levelObj.getId(), Level.convertGridToString(level), response -> {
                    levelObj.setLevel(level);
                    Toast.makeText(context, "Level Saved", Toast.LENGTH_SHORT).show();
                    dismiss();
                    ((Activity)context).setResult(LevelViewActivity.ACTIVITY_ID, new Intent().putExtra("level", levelObj));
                    ((Activity)context).finish();
                }, error -> {
                    progressBar.setVisibility(View.GONE);
                    uploadBTN.setVisibility(View.VISIBLE);
                    Toast.makeText(context, "Error has been occurred", Toast.LENGTH_SHORT).show();
                });
                return;
            }

            if(levelNameET.getText().toString().length() < 3){
                Toast.makeText(context, "Your level name should be at least 3 letters", Toast.LENGTH_SHORT).show();
                return;
            }

            progressBar.setVisibility(View.VISIBLE);
            uploadBTN.setVisibility(View.GONE);
            JsonObjectRequest obj = NetworkRequests.getInstance(context).uploadLevelRequest(levelNameET.getText().toString(), Level.convertGridToString(level), response -> {

                Toast.makeText(context, "Level Uploaded", Toast.LENGTH_SHORT).show();
                dismiss();
                ((Activity) context).finish();


            }, error -> {
                progressBar.setVisibility(View.GONE);
                uploadBTN.setVisibility(View.VISIBLE);
                Toast.makeText(context, "Error has been occurred", Toast.LENGTH_SHORT).show();
            });

            obj.setTag("UPLOAD");


        });

        cancelBTN.setOnClickListener(v -> {
            NetworkRequests.getInstance(context).getRequestQueue().cancelAll("UPLOAD");
            dismiss();
        });


        // setting the dialog fit the screen width
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(lp);


        setCancelable(false);
    }

    @Override
    public void show() {

        super.show();
    }

    @Override
    public void dismiss() {
        super.dismiss();

    }
}
