package com.avirzayev.arkanoid.networking;

import androidx.annotation.NonNull;

import java.util.HashMap;


// used to simplify the interaction to adding arguments to the url
public class Arguments {

    // hash map of arguments
    private HashMap<String, Object> args = new HashMap<>();

    // creating valid argument string for url
    public String getArgsString(){
        String result = "?";
        for (String key :
                args.keySet()) {

            result += key + "=" + args.get(key) + "&";
        }
        return result.substring(0, result.length() - 1);
    }

    // adding an argument
    public Arguments addArgument(String key, Object object){
        args.put(key,object);
        return this;
    }

    // removing an argument
    public Arguments removeArgument(String key){
        args.remove(key);

        return this;
    }

    @NonNull
    @Override
    public String toString() {
        return getArgsString();
    }
}
