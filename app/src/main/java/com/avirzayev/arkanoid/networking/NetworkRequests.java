package com.avirzayev.arkanoid.networking;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class NetworkRequests {

    // Response codes from the server
    public static final class RequestStatus {

        public static final int ERROR = -1;
        public static final int SUCCESS = 0;
        public static final int LOGIN_REQUIRED = 1;
        public static final int ACCOUNT_NOT_FOUND = 2;
        public static final int WRONG_PASSWORD = 3;
        public static final int EMAIL_TAKEN = 4;
        public static final int USERNAME_TAKEN = 5;
        public static final int EMAIL_NOT_EXISTS = 6;
        public static final int ALREADY_LOGGED = 7;

    }

    // data keys passed to identify the arguments
    public static final class ArgumentKeys {

        public static final String USER_KEY = "user";
        public static final String PASSWORD_KEY = "password";
        public static final String EMAIL_KEY = "email";
        public static final String ID_KEY = "id";
        public static final String STATUS_KEY = "status";
        public static final String LEVEL_GRID_KEY = "level_grid";
        public static final String LEVEL_NAME_KEY = "level_name";
        public static final String LEVEL_ID_KEY = "level_id";
        public static final String FILTER_KEY = "filter";

        public static final int filter_by_level_popularity = 0;
        public static final int filter_by_level_name = 1;
        public static final int filter_by_level_creator = 2;
        public static final int filter_by_new_levels = 3;

    }

    public static final String DOMAIN = "https://aqueous-shelf-54605.herokuapp.com"; // server domain

    // routes
    public static final String LOGIN = "/login";
    public static final String AUTH_CHECK = "/login/check";
    public static final String LOGOUT = "/logout";
    public static final String REGISTER = "/register";
    public static final String LEVEL_UPLOAD = "/levels/upload";
    public static final String LEVEL_PULL= "/levels/pull";
    public static final String USER_LEVEL_PULL= "/levels/pull/user";


    // current cookie, will be received from the sever
    private String cookie = "";

    // shared preferences file that the cookie will be saved
    private static final String COOKIE_STORE = "cookies";

    // key to access the cookie from the file
    private static final String COOKIE_KEY = "cookie";

    // shared preference to store the cookie
    private SharedPreferences sharedPreferences;

    // class instance
    private static NetworkRequests instance;

    // manages the request that posted to the server
    private RequestQueue requestQueue;

    // application context
    private static Context ctx;



    private NetworkRequests(Context context) {
        ctx = context;

        // creating the queue
        requestQueue = getRequestQueue();

        // initializing share preferences
        sharedPreferences = ctx.getSharedPreferences(COOKIE_STORE, Context.MODE_PRIVATE);

        // initializing the cookie
        cookie = getCookies(context);

    }

    // returning the request queue
    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {

            requestQueue = Volley.newRequestQueue(ctx.getApplicationContext());
        }
        return requestQueue;
    }

    // adding a request to the queue
    public void addToRequestQueue(JsonObjectRequest req) {
        getRequestQueue().add(req);
    }

    // returns the instance
    public static synchronized NetworkRequests getInstance(Context context) {
        if (instance == null) {
            instance = new NetworkRequests(context);
        }
        return instance;
    }

    // generates full url with the route
    public static String getFullURL(String route){
        return DOMAIN + route;
    }

    // saves the cookie
    private void saveCookie(String cookie){

        // return if null
        if (cookie == null) {
            return;
        }

        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(COOKIE_KEY, cookie);
        editor.apply();

        // updating the current cookie
        this.cookie = cookie;
    }

    // returns the cookie from SP
    public String getCookies(Context context){

        return sharedPreferences.getString(COOKIE_KEY, "");
    }

    public void clearCookie() {
        saveCookie("");
    }

    // creates a request, json object needed to pass data, url and listeners
    public JsonObjectRequest postJsonRequest(JSONObject jsonObject, String url, Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener){

        // creating the object and overriding it
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, url, jsonObject, responseListener, errorListener){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Accept","application/json");

                // putting the cookie into the header, later will be passed to the server
                if(!cookie.equals("")){
                    headers.put("Cookie", cookie);
                }

                return headers;
            }

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {

                // getting the header from the response
                Map headers = response.headers;

                // getting the cookie and saving it
                String cookie = (String)headers.get("Set-Cookie");

                saveCookie(cookie);

                return super.parseNetworkResponse(response);
            }
        };


        // adding the request to the queue
        addToRequestQueue(jsonObjectRequest);



        return jsonObjectRequest;
    }

    // custom requests that passed to the current server

    public JsonObjectRequest loginRequest(String email, String password, Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener) {

        Arguments args = new Arguments().addArgument(ArgumentKeys.EMAIL_KEY, email).addArgument(ArgumentKeys.PASSWORD_KEY, password);

        return postJsonRequest(null,getFullURL(LOGIN) + args, responseListener, errorListener);
    }

    public JsonObjectRequest checkAuthenticationRequest(Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener){
        return postJsonRequest(null, getFullURL(AUTH_CHECK), responseListener, errorListener);
    }

    public JsonObjectRequest registerRequest(String email, String username, String password, Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener){


        Arguments args = new Arguments().addArgument(ArgumentKeys.EMAIL_KEY, email).addArgument(ArgumentKeys.USER_KEY,username).
                addArgument(ArgumentKeys.PASSWORD_KEY, password);

        return postJsonRequest(null, getFullURL(REGISTER) + args, responseListener, errorListener);

    }

    public JsonObjectRequest logoutRequest(Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener){

        return postJsonRequest(null, getFullURL(LOGOUT), responseListener, errorListener);

    }

    public JsonObjectRequest uploadLevelRequest(String name, String level , Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener){
        Arguments args = new Arguments().addArgument(ArgumentKeys.LEVEL_NAME_KEY, name).addArgument(ArgumentKeys.LEVEL_GRID_KEY, level);

        return postJsonRequest(null, getFullURL(LEVEL_UPLOAD)+args, responseListener, errorListener);
    }

    public JsonObjectRequest pullLevelsByNameRequest(String name, Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener){
        Arguments args = new Arguments().addArgument(ArgumentKeys.LEVEL_NAME_KEY, name);

        return postJsonRequest(null, getFullURL(LEVEL_PULL) + args, responseListener, errorListener);
    }

    public JsonObjectRequest pullLevelByIdRequest(int id, Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener){
        Arguments args = new Arguments().addArgument(ArgumentKeys.LEVEL_ID_KEY, id);

        return postJsonRequest(null, getFullURL(LEVEL_PULL) + args, responseListener, errorListener);
    }

    public JsonObjectRequest pullLevelsByCreatorRequest(String name, Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener){
        Arguments args = new Arguments().addArgument(ArgumentKeys.USER_KEY, name);

        return postJsonRequest(null, getFullURL(LEVEL_PULL) + args, responseListener, errorListener);
    }

    public JsonObjectRequest pullLevelsOfOwnRequest(Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener){

        return postJsonRequest(null, getFullURL(USER_LEVEL_PULL), responseListener, errorListener);
    }

    public JsonObjectRequest pullLevelsByFilter(int filter, String levelName, String creator, Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener){
        Arguments args = new Arguments().addArgument(ArgumentKeys.USER_KEY, creator).addArgument(ArgumentKeys.LEVEL_NAME_KEY, levelName).addArgument(ArgumentKeys.FILTER_KEY, filter);

        return postJsonRequest(null, getFullURL(LEVEL_PULL) + args, responseListener, errorListener);
    }

    public JsonObjectRequest likeOrDislikeLevelRequest(int levelId, Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener){

        return postJsonRequest(null, DOMAIN + "/levels/"+levelId + "/setlike", responseListener, errorListener);

    }

    public JsonObjectRequest overrideLevelRequest(int levelId, String level, Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener){
        Arguments args = new Arguments().addArgument(ArgumentKeys.LEVEL_GRID_KEY, level);


        return postJsonRequest(null, DOMAIN + "/levels/"+levelId + "/override" + args, responseListener, errorListener);

    }

}
