package com.avirzayev.arkanoid.edit;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

import com.avirzayev.arkanoid.dialogs.UploadDialog;
import com.avirzayev.arkanoid.game.CustomView;
import com.avirzayev.arkanoid.game.GameView;
import com.avirzayev.arkanoid.game.Level;
import com.avirzayev.arkanoid.gameobjects.Brick;
import com.avirzayev.arkanoid.utilities.HeadUpDisplay;
import com.avirzayev.arkanoid.utilities.ImageHandler;
import com.avirzayev.arkanoid.utilities.Text;
import com.avirzayev.arkanoid.utilities.TextButton;

import static com.avirzayev.arkanoid.gameobjects.Brick.BRICK_HEIGHT;
import static com.avirzayev.arkanoid.gameobjects.Brick.BRICK_WIDTH;
import static com.avirzayev.arkanoid.utilities.HeadUpDisplay.LOWER_DIMENSIONS_Y;
import static com.avirzayev.arkanoid.utilities.HeadUpDisplay.UPPER_DIMENSIONS_HEIGHT;
import static com.avirzayev.arkanoid.utilities.HeadUpDisplay.UPPER_DIMENSIONS_WIDTH;

public class EditView extends CustomView {

    private static Bitmap background;
    private static Bitmap headsup;
    public int[][] brickGrid;
    public boolean isUploading;
    public Level level;
    private TextButton uploadBTN;
    private Bitmap brickImages[];


    public EditView(Context context) {
        super(context);

        brickGrid = new int[8][6];

        isUploading = true;

        getHolder().addCallback(this);

        setFocusable(true);

    }

    @Override
    public void update(float delta) {

    }


    @Override
    public void surfaceCreated(SurfaceHolder holder) {

        if (gameThread.isAlive()) return;

        super.surfaceCreated(holder);

        ImageHandler.loadImages(getContext());

        initGameSizes();

        background = Bitmap.createScaledBitmap(ImageHandler.getImage("wall"), (int)WIDTH+ 10, (int)HEIGHT + 10, true);
        headsup = Bitmap.createScaledBitmap(ImageHandler.getImage("headsup"), (int)UPPER_DIMENSIONS_WIDTH,(int)UPPER_DIMENSIONS_HEIGHT, false);

        brickImages = new Bitmap[]{ Bitmap.createScaledBitmap(ImageHandler.getImage("brick"), (int)BRICK_WIDTH, (int)BRICK_HEIGHT, true),

                                    Bitmap.createScaledBitmap(ImageHandler.getImage("unbreak_brick"), (int)BRICK_WIDTH, (int)BRICK_HEIGHT, true),
                                     Bitmap.createScaledBitmap(ImageHandler.getImage("hard_brick"), (int)BRICK_WIDTH, (int)BRICK_HEIGHT, true)};

        String uploadString = isUploading ?"UPLOAD":"SAVE";

        Text upload = new Text(uploadString, 0,0, 70f, Color.WHITE);
        upload.centerToWidth(0, WIDTH);
        upload.centerToHeight(LOWER_DIMENSIONS_Y, HeadUpDisplay.LOWER_DIMENSIONS_HEIGHT );

        uploadBTN = new TextButton(upload, Color.parseColor("#8a8a8a"));

        uploadBTN.setOnClickListener(v -> {

            ((Activity)getContext()).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    UploadDialog d = new UploadDialog(getContext(), brickGrid, isUploading ,level);

                    d.show();

                }
            });

        });


    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }


    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);

        if (canvas == null) return;


        canvas.drawBitmap(background,0,0, new Paint());

        canvas.drawBitmap(headsup,HeadUpDisplay.UPPER_DIMENSIONS_X, HeadUpDisplay.UPPER_DIMENSIONS_Y, null);
        canvas.drawBitmap(headsup,HeadUpDisplay.LOWER_DIMENSIONS_X, HeadUpDisplay.LOWER_DIMENSIONS_Y, null);

        float brickWidth = BRICK_WIDTH + Brick.PADDING;
        float brickHeight = BRICK_HEIGHT + Brick.PADDING;


        float startX = (GameView.WIDTH - (6*brickWidth - Brick.PADDING))/2;

        float startY = UPPER_DIMENSIONS_HEIGHT + Brick.PADDING;


        float saveStartX = startX;

        Paint p = new Paint();
        p.setStyle(Paint.Style.STROKE);
        p.setColor(Color.YELLOW);

        Paint p2 = new Paint();
        p2.setColor(Color.YELLOW);

        for(int r = 0; r < brickGrid.length; r++){
            for(int c = 0; c  < brickGrid[r].length; c++){

                if (brickGrid[r][c] != 0){
                    canvas.drawBitmap(brickImages[brickGrid[r][c] - 1], startX, startY, new Paint());
                }else{
                    canvas.drawRect(startX,startY, startX + BRICK_WIDTH, startY + BRICK_HEIGHT, p);

                }


                startX += brickWidth;
            }
            startX  = saveStartX;
            startY += brickHeight;
        }

        uploadBTN.draw(canvas);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        float ex = event.getX();

        float ey = event.getY();

        int rowIndex = (int) Math.floor(((ey- UPPER_DIMENSIONS_HEIGHT- Brick.PADDING)/(BRICK_HEIGHT + Brick.PADDING)));
        int columnIndex = (int) (ex/( BRICK_WIDTH + Brick.PADDING));

        switch(event.getAction())
        {
            case MotionEvent.ACTION_DOWN:

                break;

            case MotionEvent.ACTION_MOVE:


                break;
            case MotionEvent.ACTION_UP:

                uploadBTN.updateClick(ex,ey);

                if((rowIndex < brickGrid.length && rowIndex >= 0 ) && (columnIndex < brickGrid[rowIndex].length && columnIndex >= 0))
                    brickGrid[rowIndex][columnIndex] = (brickGrid[rowIndex][columnIndex] + 1)%4;
                break;
            default:
                break;
        }
        return true;
    }

    public void end(){
        gameThread.setRunning(false);
        gameThread.interrupt();

    }

}
