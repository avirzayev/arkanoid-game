package com.avirzayev.arkanoid.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.avirzayev.arkanoid.R;
import com.avirzayev.arkanoid.game.Level;

import java.util.LinkedList;

public class LevelGridViewAdapter extends BaseAdapter {

    private LinkedList<Level> levels;
    private Context context;
    private LayoutInflater inflater;


    public LevelGridViewAdapter(LinkedList<Level> levels, Context context) {
        this.levels = levels;
        this.context = context;
        inflater = (LayoutInflater.from(context));

    }

    @Override
    public int getCount() {
        return levels.size();
    }

    @Override
    public Object getItem(int position) {
        return levels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = inflater.inflate(R.layout.level_gridview_fragment, null);

        TextView name = view.findViewById(R.id.level_name_TV);
        TextView creator = view.findViewById(R.id.level_creator_TV);
        TextView likes = view.findViewById(R.id.level_likes_TV);

        name.setText(levels.get(position).getName());
        creator.setText(levels.get(position).getCreator());
        likes.setText(levels.get(position).getLikes() + "");

        return view;
    }

    public LinkedList<Level> getLevels() {
        return levels;
    }

    public void setLevels(LinkedList<Level> levels) {
        this.levels = levels;
    }
}
