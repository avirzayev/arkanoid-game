package com.avirzayev.arkanoid.gameobjects;

import android.graphics.Bitmap;

import com.avirzayev.arkanoid.game.GameView;
import com.avirzayev.arkanoid.game.Handler;
import com.avirzayev.arkanoid.utilities.DragEventHandler;
import com.avirzayev.arkanoid.utilities.ImageHandler;
import com.avirzayev.arkanoid.utilities.Vector;

/** Game Panel which should catch the balls and change their directions **/
public class Panel extends GameObject {

    public static float PANEL_WIDTH;
    public static float PANEL_HEIGHT;


    public Panel(Vector vector) {
        super(vector, PANEL_WIDTH, PANEL_HEIGHT, Bitmap.createScaledBitmap(ImageHandler.getImage("panel"), (int)PANEL_WIDTH, (int)PANEL_HEIGHT, true));


    }

    public Panel(float x, float y) {
        super(x, y, PANEL_WIDTH, PANEL_HEIGHT, Bitmap.createScaledBitmap(ImageHandler.getImage("panel"), (int)PANEL_WIDTH, (int)PANEL_HEIGHT, true));


    }


    @Override
    public void update(float delta) {
        super.update(delta);
    }
}
