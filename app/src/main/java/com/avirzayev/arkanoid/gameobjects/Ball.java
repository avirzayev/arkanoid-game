package com.avirzayev.arkanoid.gameobjects;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;

import com.avirzayev.arkanoid.game.GameView;
import com.avirzayev.arkanoid.game.LevelHandler;
import com.avirzayev.arkanoid.utilities.HeadUpDisplay;
import com.avirzayev.arkanoid.utilities.SoundsHandler;
import com.avirzayev.arkanoid.utilities.Vector;

import java.util.LinkedList;

import java.util.Random;

public class Ball extends GameObject {

    public static float RADIUS; // ball radius

    private LinkedList<Vector> lastVectors; // the last vectors where the balls traveled, used to create trail

    // velocity ranges
    public static final float MIN_INITIAL_VELOCITY = 700;
    public static final float MAX_INITIAL_VELOCITY = 800;

    // panel
    private Panel panel;


    public Ball(Vector vector, Panel panel) {
        super(vector, RADIUS, RADIUS, null);

        lastVectors = new LinkedList<>();

        this.panel = panel;


    }

    public Ball(float x, float y, Panel panel) {
        super(x, y, RADIUS, RADIUS, null);


        lastVectors = new LinkedList<>();

        this.panel = panel;


    }

    /** randomizes the direction of the ballSound **/
    public void randomizeVelocityDirection(){
        Random rnd = new Random();

        int[] arr = {1,-1};

        vector.setVelX(getRandomVel()*arr[rnd.nextInt(arr.length)]);
        vector.setVelY(getRandomVel()*arr[rnd.nextInt(arr.length)]);

    }

    // randomizing the current vel and reversing it
    public float randomAndInvertVel(float currentVel){
        if (currentVel == 0) return getRandomVel();
        return getRandomVel()*(-currentVel/Math.abs(currentVel));
    }

    // returns random velocity
    public float getRandomVel(){
        return new Random().nextInt((int) (MAX_INITIAL_VELOCITY - MIN_INITIAL_VELOCITY)) + MIN_INITIAL_VELOCITY;
    }

    @Override
    public void update(float delta) {
        updateTrail();

        super.update(delta);

        ballMovement();

        // handling collusion
        if(panel != null && checkCollision(panel)){
            SoundsHandler.play("ball",false);
            LevelHandler.increasePanelHitScore();
        }

    }

    // the method checks collision from the sides of the hit box and inverts the velocity when hitted
    public boolean checkCollision(GameObject object){

        if(collidesWith(object)){

            boolean collisionDetected = false;

            if(vector.getPosX() < object.getVector().getPosX() + object.getWidth() && object.getVector().getPosX() < vector.getPosX()){
                if(vector.getPosY() < object.getVector().getPosY() + object.getHeight()/2){
                    vector.setVelY(randomAndInvertVel(vector.getVelY()));
                    vector.setPosY(object.getVector().getPosY() - RADIUS);
                    collisionDetected = true;
                }else if(vector.getPosY() > object.getVector().getPosY() + object.getHeight()/2){
                    vector.setVelY(randomAndInvertVel(vector.getVelY()));
                    vector.setPosY(object.getVector().getPosY() + object.getHeight() + RADIUS);
                    collisionDetected = true;
                }
            }

            if(vector.getPosY() < object.getVector().getPosY() + object.getHeight() && object.getVector().getPosY() < vector.getPosY() && !collisionDetected){
                if(vector.getPosX() < object.getVector().getPosX() + object.getWidth()/2){
                    vector.setVelX(randomAndInvertVel(vector.getVelX()));
                    vector.setPosX(object.getVector().getPosX() - RADIUS);
                    collisionDetected = true;


                }else if(vector.getPosX() > object.getVector().getPosX() + object.getWidth()/2){
                    vector.setVelX(randomAndInvertVel(vector.getVelX()));
                    vector.setPosX(object.getVector().getPosX() + object.getWidth() + RADIUS);
                    collisionDetected = true;
                }
            }


            if(!collisionDetected){

                if(vector.getPosY() < object.getVector().getPosY() + object.getHeight()/2){
                    vector.setVelY(randomAndInvertVel(vector.getVelY()));
                    vector.setPosY(object.getVector().getPosY() - RADIUS);
                }else if(vector.getPosY() > object.getVector().getPosY() + object.getHeight()/2){
                    vector.setVelY(randomAndInvertVel(vector.getVelY()));
                    vector.setPosY(object.getVector().getPosY() + object.getHeight() + RADIUS);
                }

                Log.d("getcorn", "corner");
            }
            return true;
        }

        return false;
    }

    // updating the list of last positions of the ball, works with queue
    private void updateTrail(){

        lastVectors.add(new Vector(vector));

        if(lastVectors.size() >= 8){
            lastVectors.removeFirst();
        }
    }

    // drawing the trail
    private void drawTrail(Canvas canvas){
        Paint trail = new Paint();
        int alpha = 30;

        for (Vector o : lastVectors) {
            trail.setColor(Color.argb(alpha,220,220,220));
            canvas.drawCircle( o.getPosX(),  o.getPosY(), RADIUS, trail);

            //each trail is more faded the the other
            alpha += 80/lastVectors.size();
        }
    }


    /** handling the ballSound movement and collision in walls **/
    private void ballMovement(){
        if(vector.getPosX() - RADIUS <= 0 ){
            SoundsHandler.play("ball",false);
            vector.setVelX(randomAndInvertVel(vector.getVelX()));
            vector.setPosX( RADIUS);

        }else if(vector.getPosX() + RADIUS >= GameView.WIDTH){
            SoundsHandler.play("ball",false);
            vector.setVelX(randomAndInvertVel(vector.getVelX()));
            vector.setPosX(GameView.WIDTH - RADIUS);
        }

        if(vector.getPosY() - RADIUS <= HeadUpDisplay.UPPER_DIMENSIONS_HEIGHT){
            SoundsHandler.play("ball",false);
            vector.setVelY(randomAndInvertVel(vector.getVelY()));
            vector.setPosY( RADIUS + HeadUpDisplay.UPPER_DIMENSIONS_HEIGHT);
        }else if(vector.getPosY() + RADIUS >= GameView.HEIGHT - HeadUpDisplay.LOWER_DIMENSIONS_HEIGHT){
            vector.setVelY(randomAndInvertVel(vector.getVelY()));
            vector.setPosY(GameView.HEIGHT - HeadUpDisplay.LOWER_DIMENSIONS_HEIGHT - RADIUS);
            LevelHandler.ballLoss();
        }
    }

    @Override
    public void draw(Canvas canvas) {

        drawTrail(canvas);

        Paint p = new Paint();
        p.setColor(Color.WHITE);

        canvas.drawCircle(vector.getPosX(), vector.getPosY(), RADIUS, p);

        Paint p2 = new Paint();
        p2.setColor(Color.RED);

    }

    @Override
    public boolean intersects(float x, float y) {
        return Math.hypot(vector.getPosX()-x, vector.getPosY()-y) < RADIUS;
    }

    @Override
    public boolean collidesWith(GameObject object){
        float closeX = clamp(object.getVector().getPosX(), object.getVector().getPosX() + object.getWidth(), vector.getPosX());
        float closeY = clamp(object.getVector().getPosY(), object.getVector().getPosY() + object.getHeight(), vector.getPosY());

        return Math.hypot(vector.getPosX() - closeX, vector.getPosY() - closeY) < RADIUS;
    }

    private float clamp(float min, float max, float value){
        if(value > max) return max;
        if (value < min) return min;
        return value;
    }

}
