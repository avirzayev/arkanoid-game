package com.avirzayev.arkanoid.gameobjects;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.avirzayev.arkanoid.utilities.ImageHandler;
import com.avirzayev.arkanoid.utilities.Vector;


public class Brick extends GameObject {

    public static float BRICK_WIDTH = 0;
    public static float BRICK_HEIGHT = 0;
    public static float PADDING = 5;

    private int health = 1;



    public Brick(Vector vector) {
        super(vector, BRICK_WIDTH, BRICK_HEIGHT, null);
    }

    public Brick(float x, float y) {
        super(x, y, BRICK_WIDTH, BRICK_HEIGHT, Bitmap.createScaledBitmap(ImageHandler.getImage("brick"), (int)BRICK_WIDTH, (int)BRICK_HEIGHT, true));


    }

    public Brick(Vector vector, Bitmap bitmap) {
        super(vector, BRICK_WIDTH, BRICK_HEIGHT, bitmap);
    }

    public Brick(float x, float y, Bitmap bitmap) {
        super(x, y, BRICK_WIDTH, BRICK_HEIGHT,bitmap);


    }



    @Override
    public void update(float delta) {
        super.update(delta);

    }


    @Override
    public void draw(Canvas canvas) {
        Paint p = new Paint();

        p.setColor(Color.rgb(255,30,86));

        // checks if there is an image, handles drawing
        if (bitmap != null)
            canvas.drawBitmap(bitmap, vector.getPosX(),vector.getPosY(), p);
        else
            canvas.drawRect ( vector.getPosX(),vector.getPosY(), vector.getPosX() + width, vector.getPosY() + height, p);
    }

    public boolean isBreakable(){
        return !(this instanceof UnbreakableBrick);
    }


    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

}
