package com.avirzayev.arkanoid.gameobjects;

import android.graphics.Bitmap;

import com.avirzayev.arkanoid.utilities.ImageHandler;
import com.avirzayev.arkanoid.utilities.Vector;

public class HardBrick extends Brick {

    public static final int DEFAULT_HEALTH = 3;
    public HardBrick(Vector vector) {
        super(vector, Bitmap.createScaledBitmap(ImageHandler.getImage("hard_brick"), (int)BRICK_WIDTH, (int)BRICK_HEIGHT, true));

        setHealth(DEFAULT_HEALTH);
    }

    public HardBrick(float x, float y) {
        super(x, y, Bitmap.createScaledBitmap(ImageHandler.getImage("hard_brick"), (int)BRICK_WIDTH, (int)BRICK_HEIGHT, true));
        setHealth(DEFAULT_HEALTH);

    }
}
