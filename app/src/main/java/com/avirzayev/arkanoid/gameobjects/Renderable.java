package com.avirzayev.arkanoid.gameobjects;

import android.graphics.Canvas;

/** Defines an instance that can be shown on screen **/
public interface Renderable {

    /** updates the calculations **/
    void update(float delta);


    /** draws the result **/
    void draw(Canvas canvas);
}
