package com.avirzayev.arkanoid.gameobjects;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.avirzayev.arkanoid.game.GameView;
import com.avirzayev.arkanoid.utilities.Vector;


/** Abstract class which defines all the interactable objects in the game **/
public abstract class GameObject implements Renderable{

    protected Vector vector; // vector of the object

    protected float width, height; // dimensions

    protected Bitmap bitmap; // image


    /* ************
     constructors
     *********** */
    public GameObject(Vector vector, float width, float height,Bitmap bitmap) {
        this.vector = new Vector(vector);
        this.bitmap = bitmap;
        this.width = width;
        this.height = height;
    }

    public GameObject(float x, float y, float width, float height,Bitmap bitmap) {
        this.vector = new Vector(x,y);
        this.bitmap = bitmap;
        this.width = width;
        this.height = height;
    }

    /** avoids from getting out of screen **/
    public void clampToBoundries(float w, float h){
        clampToBoundries(0,0, w, h);
    }

    /** avoids from getting out of screen **/
    public void clampToBoundries(float x, float y, float w, float h){
        if(vector.getPosX() + width > w) vector.setPosX(w - width);
        else if (vector.getPosX() < x ) vector.setPosX(x);

        if(vector.getPosY() + height > h) vector.setPosY(h - height);
        else if (vector.getPosY() < y ) vector.setPosY(y);
    }

    /** avoids from getting out of screen **/
    public void clampToBoundries(){
        clampToBoundries(0,0, GameView.WIDTH, GameView.HEIGHT);
    }

    /* ************
     end of constructors
     *********** */


    /* ************
      override from Renderable
     *********** */
    @Override
    public void update(float delta) {
        vector.update(delta);
    }

    @Override
    public void draw(Canvas canvas) {
        Paint p = new Paint();

        // checks if there is an image, handles drawing
        if (bitmap != null)
            canvas.drawBitmap(bitmap, vector.getPosX(),vector.getPosY(), p);
        else
            canvas.drawRect ( vector.getPosX(),vector.getPosY(), vector.getPosX() + width, vector.getPosY() + height, p);
    }

    /* ************
      end of override from Renderable
     *********** */


    /** returns if the object intersects with some point **/
    public boolean intersects(float x, float y){
        return vector.getPosX() < x && vector.getPosX() + width > x && vector.getPosY() < y && vector.getPosY() + height > y;
    }

    /** returns if the is any collision with other object **/

    public boolean collidesWith(GameObject object){
        return vector.getPosX() < object.getVector().getPosX() + object.width &&
                vector.getPosX() + width > object.getVector().getPosX() &&
                vector.getPosY() < object.getVector().getPosY() + object.height &&
                vector.getPosY() + height > object.getVector().getPosY();
    }


    /* ************
     getters and setters
     *********** */

    public Vector getVector() {
        return vector;
    }

    public void setVector(Vector vector) {
        this.vector = vector;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    /* ************
     end of getters and setters
     *********** */
}
