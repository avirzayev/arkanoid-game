package com.avirzayev.arkanoid.gameobjects;

import android.graphics.Bitmap;

import com.avirzayev.arkanoid.utilities.ImageHandler;
import com.avirzayev.arkanoid.utilities.Vector;

public class UnbreakableBrick extends Brick {

    public UnbreakableBrick(Vector vector) {
        super(vector,Bitmap.createScaledBitmap(ImageHandler.getImage("unbreak_brick"), (int)BRICK_WIDTH, (int)BRICK_HEIGHT, true));
    }

    public UnbreakableBrick(float x, float y) {
        super(x, y, Bitmap.createScaledBitmap(ImageHandler.getImage("unbreak_brick"), (int)BRICK_WIDTH, (int)BRICK_HEIGHT, true));
    }
}
