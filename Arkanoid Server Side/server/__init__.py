import os

from flask import Flask, request, redirect
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy

# network
host = '10.0.0.19'
port = 9000

# database
database_path = os.path.abspath(os.getcwd()) + '/app.db'
print("lol= ", database_path)

database_uri = 'sqlite:///'+database_path

app = Flask(__name__)

# configs
app.config['SECRET_KEY'] = "4d58w34vg548pbe457pg973b4"
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ['DATABASE_URL']
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True


db = SQLAlchemy(app)
login_manager = LoginManager(app)


from server import routes


def create_app():

    login_manager.init_app(app)
    db.init_app(app)


