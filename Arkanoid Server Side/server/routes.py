from server import *
from flask import request, json
from server.models import *
from server.data_access_keys import *
from server.status_codes import *
from flask_login import current_user, login_user, logout_user, login_required


@login_manager.unauthorized_handler
@app.route("/logout",  methods=["GET", "POST"])
def logout():
    logout_user()
    return generate_status_json(success_code)

# login route
@app.route("/login", methods=["GET", "POST"])
def login():

    if current_user.is_authenticated:
        return generate_status_json(already_logged)

    # getting sent data
    data = get_request_args()

    if validate_keys(data, email_key, password_key):

        # getting user from database
        user_object = User.query.filter_by(email=data.get(email_key)).first()

        # checking for user existence
        if not user_object:
            return generate_status_json(email_not_exists)

        # checking if the passwords match
        if user_object.is_password_valid(data.get(password_key)):

            # login the user
            login_user(user_object, remember=True)

            return generate_status_json(success_code)

        return generate_status_json(wrong_password_code)

    return generate_status_json(error_code)


# register route
@app.route("/register", methods=["GET", "POST"])
def register():

    # getting sent data
    data = get_request_args()

    # checking if all the required keys inside
    if validate_keys(data, user_key, email_key, password_key):

        # avoiding duplicates of email and username
        if User.query.filter_by(email=data.get(email_key)).first():
            return generate_status_json(email_taken)

        if User.query.filter_by(username=data.get(user_key)).first():
            return generate_status_json(username_taken)

        # creating new user
        user_object = User(email=data.get(email_key), username=data.get(user_key), password=data.get(password_key))

        # adding user to the database
        db.session.add(user_object)
        db.session.commit()

        # login the user
        login_user(user_object, remember=True)

        print("created user: ", user_object)

        # sending success code
        return generate_status_json(success_code)

    # return error code for wrong keys
    return generate_status_json(error_code)


@login_required
@app.route("/levels/upload", methods=["GET", "POST"])
def upload_level():

    # getting sent data
    data = get_request_args()

    # checking if all the required keys inside
    if validate_keys(data, level_grid_key, level_name_key):

        # creating the level
        level_object = Level(name=data.get(level_name_key), grid=data.get(level_grid_key), user=current_user)

        # adding the level to the database
        db.session.add(level_object)
        db.session.commit()

        return generate_status_json(success_code)

    return generate_status_json(error_code)

@login_required
@app.route("/levels/<levelid>/override", methods=["GET", "POST"])
def update_level(levelid):

    data = get_request_args()

    if validate_keys(data, level_grid_key):
        level_object = Level.query.get(levelid)

        if level_object:
            level_object.grid = data.get(level_grid_key)
            db.session.commit()
            print(levelid)
            return generate_status_json(success_code)

    return generate_status_json(error_code)

@login_required
@app.route("/levels/pull", methods=["GET", "POST"])
def pull_levels():

    # getting data
    data = get_request_args()

    # getting the keys
    level_name_arg = data.get(level_name_key)
    # level_id_arg = data.get(level_id_key)
    level_creator_name_arg = data.get(user_key)
    filter_arg = data.get(filter_key)

    if filter_arg:
        filter_arg = int(filter_arg)

    if filter_arg == filter_by_popularity:
        if level_name_arg is None or len(level_name_arg) == 0:
            return "{%s:%d, levels:%s}" % (status_key, success_code, Level.query.order_by(Level.likes.desc()).limit(100).all())
        return "{%s:%d, levels:%s}" % (status_key, success_code, Level.query.filter_by(name=level_name_arg).order_by(Level.likes.desc()).limit(100).all())

    if filter_arg == filter_by_new_levels:
        if level_name_arg is None or len(level_name_arg) == 0:
            return "{%s:%d, levels:%s}" % (status_key, success_code, Level.query.order_by(Level.id.desc()).limit(100).all())
        return "{%s:%d, levels:%s}" % (status_key, success_code, Level.query.filter_by(name=level_name_arg).order_by(Level.id.desc()).limit(100).all())

    if filter_arg == filter_by_level_creator:
        if level_creator_name_arg:
            user_object = User.query.filter(User.username.contains(level_creator_name_arg)).limit(50).all()

            if user_object:
                levels_arr = []

                for u in user_object:
                    for l in u.created_levels:
                        levels_arr.append(l)

                return "{%s:%d, levels:%s}" % (status_key, success_code, levels_arr)


        return "{%s:%d, levels:%s}" % (status_key, success_code, [])

    if filter_arg == filter_by_level_name:
        if level_name_arg:
            levels_object = Level.query.filter(Level.name.contains(level_creator_name_arg)).limit(100).all()

            if levels_object:
                levels_arr = []

                for l in levels_object:
                    levels_arr.append(l)

                return "{%s:%d, levels:%s}" % (status_key, success_code, levels_arr)

        return "{%s:%d, levels:%s}" % (status_key, success_code,[])


    #
    # # if level name provided return all the levels with that name
    # if level_name_arg:
    #     return "{%s:%d, levels:%s}" % (status_key, success_code, Level.query.filter_by(name=level_name_arg).all())
    #
    # # if level id provided return the level
    # if level_id_arg:
    #     level = Level.query.get(int(level_id_arg))
    #
    #     # checking if the level exists
    #     if level:
    #         return "{%s:%d, level:%s}" % (status_key, success_code, level)
    #
    # # if level creator name provided return all his levels
    # if level_creator_name_arg:
    #     user_object = User.query.filter_by(username=level_creator_name_arg).first()
    #
    #     # checking if the name valid
    #     if user_object:
    #         return "{%s:%d, levels:%s}" % (status_key, success_code, user_object.created_levels)

    return generate_status_json(error_code)

@login_required
@app.route("/levels/pull/user", methods=["GET", "POST"])
def pull_user_levels():
    return "{%s:%d, levels:%s}" % (status_key, success_code, current_user.created_levels)

@login_required
@app.route("/levels/<levelid>/setlike", methods=["GET", "POST"])
def set_level_like_status(levelid):

    level_object = Level.query.get(int(levelid))

    if level_object:

        if level_object in current_user.liked_levels:
            level_object.likes -= 1
            current_user.liked_levels.remove(level_object)
        else:
            level_object.likes += 1
            current_user.liked_levels.append(level_object)

        db.session.commit()

        return "{%s:%d}" % (status_key, success_code)

    return "{%s:%d}" % (status_key, error_code)


@app.route("/login/check", methods=["GET", "POST"])
def login_check():
    if current_user.is_authenticated:
        return generate_status_json(success_code)
    return generate_status_json(error_code)



def validate_keys(data, *args):

    for key in args:
        if key not in data:
            return False

    return True


def generate_status_json(code):
    return "{%s:%d}" % (status_key, code)


def get_request_args():
    data = request.args

    return data
