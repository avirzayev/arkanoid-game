from flask_login import UserMixin, current_user
from server import db, login_manager
import bcrypt


@login_manager.user_loader
def load_user(user_id):

    if not user_id:
        return None

    return User.query.get(user_id)

liked_levels = db.Table('liked_levels',
    db.Column('level_id', db.Integer, db.ForeignKey('level.id')),
    db.Column('user_id', db.Integer, db.ForeignKey('user.id'))
)


class User(UserMixin, db.Model):

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=False, nullable=False)
    password_hash = db.Column(db.String(120), unique=False, nullable=False)

    created_levels = db.relationship('Level', backref='user', lazy=True)
    liked_levels = db.relationship('Level', secondary=liked_levels)


    def __repr__(self):
        return '{id: %d, username: "%s"}' % (self.id, self.username)

    def is_password_valid(self, password):
        print(self.password_hash)

        return bcrypt.checkpw(password.encode('utf-8'), self.password_hash.encode('utf-8'))

    @property
    def password(self):
        raise AttributeError('password not readable')

    @password.setter
    def password(self, password):
        self.password_hash = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt()).decode("utf-8", "ignore")


class Level(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), unique=False, nullable=False)
    likes = db.Column(db.Integer, unique=False, nullable=False, default=0)
    grid = db.Column(db.String(500), unique=False, nullable=False)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'),
                          nullable=False)

    def __repr__(self):
        return '{id:%d, level_name:"%s", likes:%d, level_grid:"%s", creator:"%s", liked:"%s", editable:"%s"}' % (self.id, self.name, self.likes, self.grid, User.query.get(self.user_id).username, self in current_user.liked_levels, self.user_id == current_user.id)

