from server import app, create_app, host, port

if __name__ == '__main__':
    create_app()
    app.run()